package wtf.uuh.mc.roasty.client.ui.animations

import wtf.uuh.mc.roasty.client.QueueEntryControl
import wtf.uuh.mc.roasty.client.inGameHudLoop

const val DEFAULT_ANIMATION_DURATION = 160

abstract class Animator {

    fun animate(animation: Animation): QueueEntryControl {
        val startTime = System.currentTimeMillis()
        return inGameHudLoop.post(0) {
            val now = System.currentTimeMillis()
            val remainingTime = animation.duration - (now - startTime)
            if (remainingTime < 0) {
                cancel()
                animation.onAnimationEnd()
                return@post
            }
            val linearFactor = 1f - (remainingTime / animation.duration.toFloat())
            animation.onAnimationFrameFromAnimator(transformFactor(linearFactor))
        }
    }

    abstract fun transformFactor(linearFactor: Float): Float
}
