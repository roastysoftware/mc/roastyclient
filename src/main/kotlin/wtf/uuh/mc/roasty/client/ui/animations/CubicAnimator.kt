package wtf.uuh.mc.roasty.client.ui.animations

import kotlin.math.pow

object CubicAnimator : Animator() {

    override fun transformFactor(linearFactor: Float): Float {
        return linearFactor.pow(3)
    }

}