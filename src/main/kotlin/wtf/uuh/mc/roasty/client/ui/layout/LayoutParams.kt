package wtf.uuh.mc.roasty.client.ui.layout

import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.dp

class LayoutParams(
    var topPadding: DP = 0.dp,
    var rightPadding: DP = 0.dp,
    var bottomPadding: DP = 0.dp,
    var leftPadding: DP = 0.dp,
    var topMargin: DP = 0.dp,
    var rightMargin: DP = 0.dp,
    var bottomMargin: DP = 0.dp,
    var leftMargin: DP = 0.dp,
) {

    var horizontalSizing = ViewSizing.AUTO
    var verticalSizing = ViewSizing.AUTO

    constructor(padding: DP) : this() {
        padding(padding)
    }

    constructor(padding: DP, margin: DP) : this() {
        padding(padding)
        margin(margin)
    }


    inline var totalVerticalPadding: DP
        get() = topPadding + bottomPadding
        set(value) {
            topPadding = value / 2.0
            bottomPadding = value / 2.0
        }

    inline var totalHorizontalPadding: DP
        get() = leftPadding + rightPadding
        set(value) {
            leftPadding = value / 2.0
            rightPadding = value / 2.0
        }

    inline var totalVerticalMargin: DP
        get() = topMargin + bottomMargin
        set(value) {
            topMargin = value / 2.0
            bottomMargin = value / 2.0
        }

    inline var totalHorizontalMargin: DP
        get() = leftMargin + rightMargin
        set(value) {
            leftMargin = value / 2.0
            rightMargin = value / 2.0
        }

    /**
     * Behaves like CSS
     */
    fun padding(top: DP, right: DP? = null, bottom: DP? = null, left: DP? = null) {
        topPadding = top
        rightPadding = right ?: topPadding
        bottomPadding  = bottom ?: topPadding
        leftPadding = left ?: rightPadding
    }
    
    fun padding(layoutParams: LayoutParams) {
        topPadding = layoutParams.topPadding
        rightPadding = layoutParams.rightPadding
        bottomPadding = layoutParams.bottomPadding
        leftPadding = layoutParams.leftPadding
    }

    /**
     * Behaves like CSS
     */
    fun margin(top: DP, right: DP? = null, bottom: DP? = null, left: DP? = null) {
        topMargin = top
        rightMargin = right ?: topMargin
        bottomMargin  = bottom ?: topMargin
        leftMargin = left ?: rightMargin
    }

    fun margin(layoutParams: LayoutParams) {
        topMargin = layoutParams.topMargin
        rightMargin = layoutParams.rightMargin
        bottomMargin = layoutParams.bottomMargin
        leftMargin = layoutParams.leftMargin
    }



}