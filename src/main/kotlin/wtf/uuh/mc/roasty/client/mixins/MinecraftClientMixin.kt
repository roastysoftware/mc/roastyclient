package wtf.uuh.mc.roasty.client.mixins

import net.minecraft.client.MinecraftClient
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo
import wtf.uuh.mc.roasty.client.mainLoop

@Mixin(MinecraftClient::class)
class MinecraftClientMixin {

    @Inject(
        at = [At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/MinecraftClient;endMonitor(ZLnet/minecraft/util/TickDurationMonitor;)V",
            ordinal = 0
        )], method = ["run()V"]
    )
    fun loop(ci: CallbackInfo?) {
        mainLoop.runQueue()
    }

}