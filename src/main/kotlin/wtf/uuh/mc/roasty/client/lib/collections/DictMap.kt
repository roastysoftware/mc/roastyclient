package wtf.uuh.mc.roasty.client.lib.collections

import java.util.*
import java.util.function.Consumer

typealias DictMapObserver<K, V> = (key: K, value: V, put: Boolean) -> Unit

open class DictMap<K, V> : AbstractMap<K, V>() {
    private val set = DictMapSet<K, V>()
    private val observers = LinkedList<DictMapObserver<K, V>>()

    override operator fun get(key: K): V? = getValue(key)
    operator fun set(key: K, value: V) = put(key, value)
    operator fun minusAssign(key: K) {
        remove(key)
    }
    operator fun contains(key: K) = containsKey(key)

    override val size: Int
        get() = set.size

    fun getValue(key: K): V? {
        return set[key]
    }

    fun getInverse(value: V): K? {
        return set.getInverse(value)
    }

    override fun put(key: K, value: V): V {
        set.put(key, value)
        onChange(key, value, true)
        return value
    }

    override fun remove(key: K): V? {
        val index = set.indexOfFirst { it.key == key }
        if (index == -1) {
            return null
        }
        val value = this[key]!!
        set.removeAt(index)
        onChange(key, value, false)
        return value
    }

    override fun containsValue(value: V): Boolean {
        return set.containsValueObject(value)
    }

    override fun containsKey(key: K): Boolean {
        return set.containsKeyObject(key)
    }

    fun observe(observer: DictMapObserver<K, V>) {
        observers += observer
    }

    fun stopObserving(observer: DictMapObserver<K, V>) {
        observers -= observer
    }

    fun onChange(key: K, value: V, put: Boolean) {
        observers.forEach { it(key, value, put) }
    }

    fun addAll(vararg objects: Any) {
        require(objects.size % 2 != 1) { "Need an even number of vararg arguments (K, V, K, V, ...)" }
        var i = 0
        while (i < objects.size) {
            try {
                put(objects[i] as K, objects[i + 1] as V)
            } catch (e: ClassCastException) {
                throw RuntimeException("Mismatch in K or V", e)
            }
            i += 2
        }
    }

    class DictMapEntry<K, V>(var key: K, var value: V)
    class DictMapSet<K, V> : AbstractSet<MutableMap.MutableEntry<K, V>>() {
        private val keys = LinkedList<K>()
        private val values = LinkedList<V>()
        override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, V>> {
            return DictMapSetIterator(this)
        }

        override val size get() =
            synchronized(this) { keys.size }

        fun put(key: K, value: V) {
            synchronized(this) {
                if (keys.contains(key)) {
                    values.set(keys.indexOf(key), value)
                } else {
                    keys.add(key)
                    values.add(value)
                }
            }
        }

        fun containsKeyObject(o: Any?): Boolean {
            synchronized(this) { return keys.contains(o) }
        }

        fun containsValueObject(o: Any?): Boolean {
            synchronized(this) { return values.contains(o) }
        }

        fun containsKey(key: K): Boolean {
            synchronized(this) { return keys.contains(key) }
        }

        fun containsValue(value: V): Boolean {
            synchronized(this) { return values.contains(value) }
        }

        operator fun get(key: K): V? {
            synchronized(this) {
                val index = keys.indexOf(key)
                if (index == -1) {
                    return null
                }
                return values[index]
            }
        }

        fun getInverse(value: V): K? {
            synchronized(this) {
                val index = values.indexOf(value)
                if (index == -1) {
                    return null
                }
                return keys[index]
            }
        }

        fun removeWithKey(key: K): Boolean {
            synchronized(this) {
                if (!keys.contains(key)) {
                    return false
                }
                val index = keys.indexOf(key)
                keys.removeAt(index)
                values.removeAt(index)
            }
            return true
        }

        fun removeAt(index: Int) {
            synchronized(this) {
                keys.removeAt(index)
                values.removeAt(index)
            }
        }

        class DictMapSetIterator<K, V>(private val set: DictMapSet<K, V>) : MutableIterator<MutableMap.MutableEntry<K, V>> {
            private var currentIndex = 0
            override fun hasNext(): Boolean {
                return currentIndex < set.keys.size
            }

            override fun next(): DictMapSetEntry<K, V> {
                return DictMapSetEntry(set, currentIndex++)
            }

            override fun remove() {
                set.removeAt(currentIndex)
            }

            override fun forEachRemaining(consumer: Consumer<in MutableMap.MutableEntry<K, V>>) {
                while (hasNext()) {
                    consumer.accept(next())
                }
            }
        }

        class DictMapSetEntry<K, V>(private val set: DictMapSet<K, V>, private val index: Int) :
            MutableMap.MutableEntry<K, V> {
            override val key: K
                get() {
                    synchronized(set) { return set.keys[index] }
                }
            override val value: V
                get() {
                    synchronized(set) { return set.values[index] }
                }

            override fun setValue(v: V): V {
                synchronized(set) { set.values.set(index, v) }
                return v
            }

            override fun equals(other: Any?): Boolean {
                if (other is DictMapSetEntry<*, *>) {
                    synchronized(set) { return other.key === key && other.value === value }
                }
                return value === other
            }

            override fun hashCode(): Int {
                var result = set.hashCode()
                result = 31 * result + index
                return result
            }

        }

    }

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>>
        get() = set
}