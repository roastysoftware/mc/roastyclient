package wtf.uuh.mc.roasty.client.ui.windowing.theming

import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.layout.LayoutParams
import wtf.uuh.mc.roasty.client.ui.windowing.decorations.WindowDecoration

class DefaultWindowDecoration : WindowDecoration() {
    override val windowBorderThickness = 0.dp
    override val titleBarParams = LayoutParams().apply {
        margin(0.dp, 0.dp, 4.dp)
        padding(4.dp, 2.dp)
    }
    override val windowPadding = 2.dp
    override val backgroundColor = ColorComponents(alpha = .72f)
    override val titleBarBackgroundColor = ColorComponents(.06f, .06f, .06f)
    override val textColor = ColorComponents.white
    override val titleBarTextColor = ColorComponents.white
    override val shouldShowTitleBar = true
}