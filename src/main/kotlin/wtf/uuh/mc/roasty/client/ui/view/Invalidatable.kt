package wtf.uuh.mc.roasty.client.ui.view

interface Invalidatable {
    fun invalidate()
}