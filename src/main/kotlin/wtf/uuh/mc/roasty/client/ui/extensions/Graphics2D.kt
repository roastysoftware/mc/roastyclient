package wtf.uuh.mc.roasty.client.ui.extensions

import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import java.awt.Font
import java.awt.Graphics2D
import java.awt.RenderingHints

fun Graphics2D.applyOptimizedRendering() {
    setRenderingHint(
        RenderingHints.KEY_ALPHA_INTERPOLATION,
        RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY
    )
    setRenderingHint(
        RenderingHints.KEY_TEXT_ANTIALIASING,
        RenderingHints.VALUE_TEXT_ANTIALIAS_ON
    )
    setRenderingHint(
        RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON
    )
    setRenderingHint(
        RenderingHints.KEY_COLOR_RENDERING,
        RenderingHints.VALUE_COLOR_RENDER_QUALITY
    )
    setRenderingHint(
        RenderingHints.KEY_FRACTIONALMETRICS,
        RenderingHints.VALUE_FRACTIONALMETRICS_ON
    )
    setRenderingHint(
        RenderingHints.KEY_INTERPOLATION,
        RenderingHints.VALUE_INTERPOLATION_BILINEAR
    )
    setRenderingHint(
        RenderingHints.KEY_RENDERING,
        RenderingHints.VALUE_RENDER_QUALITY
    )
    setRenderingHint(
        RenderingHints.KEY_STROKE_CONTROL,
        RenderingHints.VALUE_STROKE_PURE
    )
}

fun Graphics2D.drawText(
    text: String, font: Font, size: Float, color: ColorComponents = ColorComponents.white,
    offs: Point = Point.zero
) {
    this.font = font.deriveScaled(size)
    this.color = color.awtColor
    drawString(text, offs.x.absInt + 0, offs.y.absInt + this.fontMetrics.maxAscent - 1)
}

inline fun Graphics2D.use(fn: Graphics2D.() -> Unit) {
    run(fn)
    dispose()
}
