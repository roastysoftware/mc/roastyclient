package wtf.uuh.mc.roasty.client.ui.input

import net.minecraft.client.util.InputUtil
import org.lwjgl.glfw.GLFW

object KeyboardInputDispatcher {

    private val listeners = ArrayList<KeyboardEventListener>()

    private fun onRightControlPressed() {
        listeners.forEach { it.onRightControlPressed() }
    }

    fun addKeyboardEventListener(listener: KeyboardEventListener) {
        listeners += listener
    }

    fun removeKeyboardEventListener(listener: KeyboardEventListener) {
        listeners -= listener
    }

    fun onKeyPressed(key: InputUtil.Key) {
        if (key.code == GLFW.GLFW_KEY_RIGHT_CONTROL) {
            onRightControlPressed()
        }
    }

}