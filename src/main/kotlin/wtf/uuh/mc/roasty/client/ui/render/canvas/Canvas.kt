package wtf.uuh.mc.roasty.client.ui.render.canvas

import wtf.uuh.mc.roasty.client.graphics.g2d.newGraphics2D
import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.drawable.Bitmap
import wtf.uuh.mc.roasty.client.ui.extensions.applyOptimizedRendering
import wtf.uuh.mc.roasty.client.ui.extensions.drawText
import wtf.uuh.mc.roasty.client.ui.geometry.*
import wtf.uuh.mc.roasty.client.ui.render.text.FontLoader
import wtf.uuh.mc.roasty.client.ui.render.text.TextSizing

const val DEBUG_DISABLE_DRAW = false

class Canvas(
    val width: DP,
    val height: DP,
    drawFn: (Canvas.() -> Unit)? = null,
) {

    private val g2dPair by lazy { newGraphics2D(width.absInt, height.absInt) }
    private val g2d by lazy { g2dPair.second.apply { applyOptimizedRendering() } }
    private inline val img get() = g2dPair.first

    var color: ColorComponents
        get() = ColorComponents.fromAwt(g2d.color)
        set(value) { g2d.color = value.awtColor }

    var backgroundColor
        get() = ColorComponents.fromAwt(g2d.background)
        set(value) { g2d.background = value.awtColor }

    val bitmap: Bitmap
        get() {
            val image = img.getRGB(
                0, 0, img.width, img.height,
                IntArray(img.width * img.height), 0, img.width)
            return Bitmap(image, img.width.px, img.height.px)
        }

    init {
        drawFn?.let { run(it) }
    }

    fun text(text: String, size: DP, color: ColorComponents) = g2d.run {
        if (DEBUG_DISABLE_DRAW) {
            return
        }
        val offs = Point()
        text.split("\n").forEach {
            drawText(it, FontLoader.main, size.dpFloat, color, offs)
            offs.y += TextSizing.textHeight(it, size)
        }
    }

    fun rectangle(
        width: DP, height: DP,
        fill: ColorComponents = ColorComponents.transparent,
        radius: DP = 0.dp
    ) {
        if (DEBUG_DISABLE_DRAW) {
            return
        }
        color = fill
        if (radius > 0.dp) {
            g2d.drawRoundRect(0, 0, width.absInt, height.absInt, radius.absInt, radius.absInt)
            g2d.fillRoundRect(0, 0, width.absInt, height.absInt, radius.absInt, radius.absInt)
        } else {
            g2d.drawRect(0, 0, width.absInt, height.absInt)
            g2d.fillRect(0, 0, width.absInt, height.absInt)
        }
    }

    inline operator fun invoke(drawFn: Canvas.() -> Unit) {
        run(drawFn)
    }

}

fun textHeight(text: String, size: DP): DP {
    return text.split("\n")
        .map { TextSizing.textHeight(it, size) }
        .reduce { acc, dp -> acc + dp }
}

fun textWidth(text: String, size: DP): DP {
    return text.split("\n")
        .map { TextSizing.textWidth(it, size) }
        .reduce { acc, dp -> dp.coerceAtLeast(acc) }
}