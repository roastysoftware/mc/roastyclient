package wtf.uuh.mc.roasty.client.ui.animations.specific

import wtf.uuh.mc.roasty.client.ui.animations.Animatable
import wtf.uuh.mc.roasty.client.ui.animations.AnimationLike
import wtf.uuh.mc.roasty.client.ui.animations.OnAnimationEndListener
import wtf.uuh.mc.roasty.client.ui.animations.animation
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import java.util.*
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class AnimatedPoint(private var from: Point = Point.zero) :
    Animatable, AnimationLike<AnimatedPoint>, ReadWriteProperty<Any, Point> {

    private var target: Point = Point.zero
    private var to: Point = Point.zero
    private val animation by lazy {
        animation().apply { addOnAnimationEndListener { this@AnimatedPoint.onAnimationEnd() } }
    }
    private val animationEndListeners = LinkedList<OnAnimationEndListener>()

    override fun onAnimationFrame(factor: Float) {
        val deltaX = to.x - from.x
        val deltaY = to.y - from.y
        target.x = from.x + deltaX * factor.toDouble()
        target.y = from.y + deltaY * factor.toDouble()
    }

    override fun animate() = apply {
        animation.animate()
    }

    override fun reverse() = apply {
        animation.reverse()
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Point) {
        from = target
        to = value
        animation.reset()
        animation.animate()
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): Point {
        return target
    }

    override fun addOnAnimationEndListener(listener: OnAnimationEndListener) {
        animationEndListeners += listener
    }

    override fun onAnimationEnd() {
        animationEndListeners.forEach { it() }
    }
}