package wtf.uuh.mc.roasty.client.ui.windowing.decorations

import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.layout.LayoutParams

abstract class WindowDecoration {

    abstract val windowPadding: DP
    abstract val windowBorderThickness: DP
    abstract val titleBarParams: LayoutParams

    abstract val shouldShowTitleBar: Boolean
    abstract val backgroundColor: ColorComponents
    abstract val titleBarBackgroundColor: ColorComponents
    abstract val textColor: ColorComponents
    abstract val titleBarTextColor: ColorComponents

}
