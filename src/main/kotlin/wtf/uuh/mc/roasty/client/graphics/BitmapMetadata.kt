package wtf.uuh.mc.roasty.client.graphics

data class BitmapMetadata(
    val format: PixelFormat,
)