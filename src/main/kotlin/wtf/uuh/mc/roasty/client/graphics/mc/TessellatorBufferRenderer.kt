package wtf.uuh.mc.roasty.client.graphics.mc

import net.minecraft.client.render.BufferBuilder
import net.minecraft.client.render.BufferRenderer
import net.minecraft.client.render.Tessellator
import net.minecraft.client.render.VertexFormat

object TessellatorBufferRenderer {

    inline fun render(mode: VertexFormat.DrawMode, vf: VertexFormat, runnable: BufferBuilder.() -> Unit) {
        val bufferBuilder = Tessellator.getInstance().buffer
        bufferBuilder.run {
            begin(mode, vf)
            run(runnable)
            end()
        }
        if (bufferBuilder.isBuilding) {
            BufferRenderer.drawWithoutShader(bufferBuilder.endNullable())
        }
    }

}