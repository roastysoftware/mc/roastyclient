package wtf.uuh.mc.roasty.client.ui.animations

import kotlin.math.pow

object QuadAnimator : Animator() {

    override fun transformFactor(linearFactor: Float): Float {
        return linearFactor.pow(2)
    }

}