package wtf.uuh.mc.roasty.client.lib.collections

class IntDictMap<V> : DictMap<Int?, V>(), OfAble<V, IntDictMap<V>?> {
    private var next = 0
    override fun add(value: V) {
        put(next++, value)
    }
}