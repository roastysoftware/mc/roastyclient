package wtf.uuh.mc.roasty.client.ui.geometry

class Box(
    var pos: Point = Point.zero,
    var size: Rectangle = Rectangle(0.dp, 0.dp),
    var depth: Double = 0.0
) {

    var minX by pos::x
    var minY by pos::y
    inline var maxX
        get() = minX + size.width
        set(value) {
            size.width = value - minX
        }

    inline var maxY
        get() = minY + size.height
        set(value) {
            size.height = value - minY
        }

    fun coerceIn(min: Box, max: Box): Box {
        if (!intersectsWith(Box(min.pos).apply{ maxX = max.maxX; maxY = max.maxY})) {
            return Box(Point.zero, Rectangle(0.dp, 0.dp), 0.0)
        }
        val box = coerceAtLeast(min)
        box.maxX = maxX.coerceAtMost(max.maxX)
        box.maxY = maxY.coerceAtMost(max.maxY)
        return box
    }

    fun coerceAtLeast(min: Box) = Box(
        Point(pos.x.coerceAtLeast(min.pos.x), pos.y.coerceAtLeast(min.pos.y)),
        depth = depth
    )

    fun coerceAtMost(max: Box): Box {
        val box = Box(pos, depth = depth)
        box.maxX = maxX.coerceAtMost(max.maxX)
        box.maxY = maxY.coerceAtMost(max.maxY)
        return box
    }

    infix fun intersectsWith(other: Box): Boolean {
        return other.minX <= maxX && other.minY <= maxY &&
                other.maxX >= minX && other.maxY >= minY
    }

}