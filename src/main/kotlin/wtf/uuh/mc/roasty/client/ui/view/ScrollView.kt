package wtf.uuh.mc.roasty.client.ui.view

import wtf.uuh.mc.roasty.client.extensions.msHaveNotPassed
import wtf.uuh.mc.roasty.client.extensions.msHavePassed
import wtf.uuh.mc.roasty.client.extensions.then
import wtf.uuh.mc.roasty.client.ui.animations.specific.AnimatedPoint
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.geometry.abs
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.layout.ViewSizing
import java.lang.IllegalStateException
import java.lang.System.currentTimeMillis
import kotlin.math.max

const val scrollSpeed = 2.0
const val accelerationMultiplier = 2.0
const val maxAcceleration = 8.0
const val accelerationTimeThreshold = 34L

class ScrollView : ViewGroup() {

    override val isScrollReceiver = true
    private var horizontalOffset = 0.dp
    private var verticalOffset = 0.dp
    private var currentHorizontalMultiplier = 1.0
    private var currentVerticalMultiplier = 1.0
    private var lastHorizontalScroll = 0L
    private var lastVerticalScroll = 0L

    override fun onMouseScroll(horizontal: DP, vertical: DP) {
        val scrollHorizontal = horizontal.dpInt != 0
        val accelerateHorizontal = scrollHorizontal && lastHorizontalScroll msHaveNotPassed accelerationTimeThreshold
        if (accelerateHorizontal && currentHorizontalMultiplier < maxAcceleration) {
            currentHorizontalMultiplier *= accelerationMultiplier
        } else {
            currentHorizontalMultiplier = 1.0
        }

        val scrollVertical = vertical.dpInt != 0
        val accelerateVertical = scrollVertical && lastVerticalScroll msHaveNotPassed accelerationTimeThreshold
        if (accelerateVertical && currentVerticalMultiplier < maxAcceleration) {
            currentVerticalMultiplier *= accelerationMultiplier
        } else {
            currentVerticalMultiplier = 1.0
        }

        if (scrollHorizontal) {
            horizontalOffset += horizontal * scrollSpeed * currentHorizontalMultiplier
            horizontalOffset = -(-horizontalOffset).coerceIn(0.dp, invisibleWidth)
            if (abs(horizontalOffset) < 1.dp) {
                currentHorizontalMultiplier = 1.0
            }
            lastHorizontalScroll = currentTimeMillis()
        }

        if (scrollVertical) {
            verticalOffset += vertical * scrollSpeed * currentVerticalMultiplier
            verticalOffset = -(-verticalOffset).coerceIn(0.dp, invisibleHeight)
            if (abs(verticalOffset) < 1.dp) {
                currentVerticalMultiplier = 1.0
            }
            lastVerticalScroll = currentTimeMillis()
        }

        additionalInnerOffset = Point(horizontalOffset, verticalOffset)
    }

    private inline val invisibleWidth get() =
        (if (children.isNotEmpty()) children[0].width - width else 0.dp).coerceAtLeast(0.dp)
    private inline val invisibleHeight get() =
        (if (children.isNotEmpty()) children[0].height - height else 0.dp).coerceAtLeast(0.dp)

    override var additionalInnerOffset by AnimatedPoint()
        private set

    override fun addView(view: View) {
        if (childrenCount == 1) {
            throw IllegalStateException("${::ScrollView.name} can only have one child")
        }
        super.addView(view)
    }

    init {
        layout {
            parameters {
                verticalSizing = ViewSizing.FILL
                horizontalSizing = ViewSizing.FILL
            }
        }
    }

}