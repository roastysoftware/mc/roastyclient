package wtf.uuh.mc.roasty.client.ui.render.text

import wtf.uuh.mc.roasty.client.resources.ResourcePath

object FontRepository {
    val ROBOTO = ResourcePath("/assets/fonts/Roboto-Regular.ttf")
}