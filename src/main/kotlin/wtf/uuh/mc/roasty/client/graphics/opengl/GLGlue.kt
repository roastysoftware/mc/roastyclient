package wtf.uuh.mc.roasty.client.graphics.opengl

import org.lwjgl.opengl.GL11.GL_RGBA
import org.lwjgl.opengl.GL11.GL_RGBA8
import org.lwjgl.opengl.GL12.GL_BGRA
import org.lwjgl.opengl.GL12.GL_UNSIGNED_INT_8_8_8_8_REV
import wtf.uuh.mc.roasty.client.exceptions.UnsupportedPixelFormatException
import wtf.uuh.mc.roasty.client.graphics.PixelFormat

object PixelFormatGlue {
    inline val PixelFormat.glInternalFormat: Int get() =
        when (this) {
            PixelFormat.RGBA_PACKED -> GL_RGBA8
            PixelFormat.BGRA_PACKED -> GL_RGBA8
        }

    inline val PixelFormat.glFormat: Int get() =
        when (this) {
            PixelFormat.RGBA_PACKED -> GL_RGBA
            PixelFormat.BGRA_PACKED -> GL_BGRA
        }

    inline val PixelFormat.glPixelFormatType: Int get() =
        when (this) {
            PixelFormat.RGBA_PACKED -> GL_UNSIGNED_INT_8_8_8_8_REV
            PixelFormat.BGRA_PACKED -> GL_UNSIGNED_INT_8_8_8_8_REV
        }
}