package wtf.uuh.mc.roasty.client.ui.animations

interface Animatable {
    fun onAnimationFrame(factor: Float)
    fun onAnimationEnd() {}
}
