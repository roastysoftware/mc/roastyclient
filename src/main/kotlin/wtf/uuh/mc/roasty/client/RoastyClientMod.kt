package wtf.uuh.mc.roasty.client

import net.fabricmc.api.ModInitializer
import wtf.uuh.mc.roasty.client.logging.logger
import wtf.uuh.mc.roasty.client.resources.ResourcePath
import wtf.uuh.mc.roasty.client.resources.loadJSONAs
import wtf.uuh.mc.roasty.client.ui.input.KeyboardEventListener
import wtf.uuh.mc.roasty.client.ui.input.KeyboardInputDispatcher
import wtf.uuh.mc.roasty.client.ui.windowing.Window
import wtf.uuh.mc.roasty.client.ui.windowing.WindowManager

data class ModInfo(
    val id: String,
    val name: String,
    val version: String
)

val modInfo: ModInfo by lazy { ResourcePath("/fabric.mod.json") loadJSONAs ModInfo::class }

object RoastyClientMod : ModInitializer, KeyboardEventListener {

    private val inGameWindowManager: WindowManager by lazy { WindowManager() }

    override fun onInitialize() {
        logger.info("Initializing ${modInfo.name} (${modInfo.id}) Version ${modInfo.version}")

        Window(inGameWindowManager).open()

        KeyboardInputDispatcher.addKeyboardEventListener(this)
    }

    override fun onRightControlPressed() {
        inGameWindowManager.show()
    }


}
