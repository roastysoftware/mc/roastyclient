package wtf.uuh.mc.roasty.client.lib.collections

interface OfAble<T, Self> {
    fun of(vararg elements: T): Self {
        for (element in elements) {
            add(element)
        }
        return this as Self
    }

    fun add(element: T)
}