package wtf.uuh.mc.roasty.client.ui.render

import net.minecraft.client.render.VertexFormat
import net.minecraft.client.render.VertexFormats.POSITION_COLOR
import net.minecraft.client.util.math.MatrixStack
import org.lwjgl.opengl.GL11.*
import wtf.uuh.mc.roasty.client.graphics.mc.TessellatorBufferRenderer.render
import wtf.uuh.mc.roasty.client.graphics.opengl.GL.disable
import wtf.uuh.mc.roasty.client.graphics.opengl.GL.enable
import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.geometry.Rectangle
import wtf.uuh.mc.roasty.client.ui.windowing.WindowRenderContext

/**
 * Runs things after pushing and manipulating a matrixStack
 */
typealias Runner = () -> Unit

object RenderImpl {

    inline fun runTranslated(
        ctx: WindowRenderContext,
        x: DP,
        y: DP,
        runner: Runner
    ) {
        ctx.matrixStack.push()
        ctx.matrixStack.translate(x.dp, y.dp, 0.0)
        runner()
        ctx.matrixStack.pop()
    }

    inline fun runTranslated(
        matrixStack: MatrixStack,
        pos: Point,
        runner: Runner
    ) {
        matrixStack.push()
        matrixStack.translate(pos.x.dp, pos.y.dp, 0.0)
        runner()
        matrixStack.pop()
    }

    fun drawRectangle(
        matrixStack: MatrixStack,
        rectangle: Rectangle,
        color: ColorComponents
    ) {

        val r = color.red
        val g = color.green
        val b = color.blue
        val a = color.alpha
        val matrix = matrixStack.peek().positionMatrix
        val x2 = rectangle.width.dpFloat
        val y2 = rectangle.height.dpFloat

        enable(GL_BLEND) {
            render(VertexFormat.DrawMode.QUADS, POSITION_COLOR) {
                vertex(matrix, 0f, y2, 0f).color(r, g, b, a).next()
                vertex(matrix, x2, y2, 0f).color(r, g, b, a).next()
                vertex(matrix, x2, 0f, 0f).color(r, g, b, a).next()
                vertex(matrix, 0f, 0f, 0f).color(r, g, b, a).next()
            }
        }

    }
}