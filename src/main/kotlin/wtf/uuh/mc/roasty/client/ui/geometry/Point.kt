package wtf.uuh.mc.roasty.client.ui.geometry

data class Point(var x: DP = 0.dp, var y: DP = 0.dp) {

    operator fun unaryPlus() = Point(+x, +y)
    operator fun unaryMinus() = Point(-x, -y)
    operator fun plus(other: Point) = Point(x + other.x, y + other.y)
    operator fun minus(other: Point) = Point(x - other.x, y - other.y)
    operator fun times(other: Point) = Point(x * other.x, y * other.y)
    operator fun div(other: Point) = Point(x / other.x, y / other.y)

    operator fun plusAssign(other: Point) {
        x = x.plus(other.x)
        y = y.plus(other.y)
    }

    operator fun minusAssign(other: Point) {
        x = x.minus(other.x)
        y = y.minus(other.y)
    }

    operator fun timesAssign(other: Point) {
        x = x.times(other.x)
        y = y.times(other.y)
    }

    operator fun divAssign(other: Point) {
        x = x.div(other.x)
        y = y.div(other.y)
    }

    fun add(both: DP) = apply {
        x = x.plus(both)
        y = y.plus(both)
    }

    fun add(point: Point) = apply { this += point }
    fun subtract(point: Point) = apply { this -= point }
    fun multiply(point: Point) = apply { this *= point }
    fun divide(point: Point) = apply { this /= point }

    fun clamp(
        minX: DP,
        minY: DP,
        maxX: DP,
        maxY: DP
    ) = apply {
        x = x.coerceIn(minX, maxX.coerceAtLeast(minX))
        y = y.coerceIn(minY, maxY.coerceAtLeast(minY))
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    override operator fun equals(other: Any?): Boolean {
        if (other !is Point) {
            return false
        }
        return x == other.x && y == other.y
    }

    fun copy() = Point(x, y)

    override fun toString(): String {
        return "{$x,$y}"
    }

    companion object {
        val zero get() = Point()
    }

}