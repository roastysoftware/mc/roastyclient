package wtf.uuh.mc.roasty.client.ui.geometry

data class Rectangle(var width: DP, var height: DP) {

    fun copy(): Rectangle {
        return Rectangle(width, height)
    }

    operator fun unaryPlus() = Rectangle(+width, +height)
    operator fun unaryMinus() = Rectangle(-width, -height)

    operator fun plus(other: Rectangle) = Rectangle(width + other.width, height + other.height)
    operator fun minus(other: Rectangle) = Rectangle(width - other.width, height - other.height)
    operator fun times(other: Rectangle) = Rectangle(width * other.width, height * other.height)
    operator fun div(other: Rectangle) = Rectangle(width / other.width, height / other.height)

    operator fun plusAssign(other: Rectangle) {
        width = width.plus(other.width)
        height = height.plus(other.height)
    }

    operator fun minusAssign(other: Rectangle) {
        width = width.minus(other.width)
        height = height.minus(other.height)
    }

    operator fun timesAssign(other: Rectangle) {
        width = width.times(other.width)
        height = height.times(other.height)
    }

    operator fun divAssign(other: Rectangle) {
        width = width.div(other.width)
        height = height.div(other.height)
    }

    fun center(width: Boolean, height: Boolean): Point {
        return Point(if (width) this.width / 2.dp else 0.dp, if (height) this.height / 2.dp else 0.dp)
    }

    val center: Point
        get() = center(width = true, height = true)

    override operator fun equals(other: Any?): Boolean {
        if (other !is Rectangle) {
            return false
        }
        return width == other.width && height == other.height
    }
}