package wtf.uuh.mc.roasty.client.extensions

inline infix fun Boolean.then(fn: () -> Unit): Boolean {
    if (this) {
        fn()
    }
    return this
}

inline infix fun Boolean.otherwise(fn: () -> Unit) {
    if (!this) {
        fn()
    }
}