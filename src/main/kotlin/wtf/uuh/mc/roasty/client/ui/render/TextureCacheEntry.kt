package wtf.uuh.mc.roasty.client.ui.render

const val TEXTURE_EXPIRES_AFTER_MS = 2000

data class TextureCacheEntry(
    val textureId: Int,
    var lastUsed: Long = System.currentTimeMillis(),
    var permanent: Boolean = false,
)