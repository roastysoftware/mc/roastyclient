package wtf.uuh.mc.roasty.client.ui.geometry

enum class HorizontalAlignment {
    LEFT, CENTER, RIGHT
}