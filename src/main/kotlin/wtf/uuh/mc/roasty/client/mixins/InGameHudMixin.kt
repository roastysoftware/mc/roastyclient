package wtf.uuh.mc.roasty.client.mixins

import com.mojang.blaze3d.systems.RenderSystem.assertOnRenderThread
import net.minecraft.client.gui.hud.InGameHud
import net.minecraft.client.util.math.MatrixStack
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable
import wtf.uuh.mc.roasty.client.inGameHudLoop
import wtf.uuh.mc.roasty.client.ui.windowing.WindowManager

@Mixin(InGameHud::class)
class InGameHudMixin {

    @Inject(
        at = [At(
            value = "INVOKE",
            target = "renderStatusEffectOverlay(Lnet/minecraft/client/util/math/MatrixStack;)V",
            ordinal = 0
        )], method = ["render(Lnet/minecraft/client/util/math/MatrixStack;F)V"]
    )
    fun render(matrixStack: MatrixStack, delta: Float, ci: CallbackInfo) {
        assertOnRenderThread()
        WindowManager.onRender(matrixStack)
        inGameHudLoop.runQueue()
    }

}