package wtf.uuh.mc.roasty.client.ui.layout

import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.geometry.sumOf
import wtf.uuh.mc.roasty.client.ui.render.DrawContext
import wtf.uuh.mc.roasty.client.ui.view.View
import wtf.uuh.mc.roasty.client.ui.view.ViewGroup
import wtf.uuh.mc.roasty.client.ui.view.drawDebugOverlay

class LinearLayout(private val orientation: Orientation = Orientation.VERTICAL) : ViewGroup() {
    override fun getViewPos(view: View): Point {
        val viewPos = super.getViewPos(view)
        for (child in children) {
            if (child === view) {
                break
            }
            viewPos += when (orientation) {
                Orientation.VERTICAL -> Point(y = child.outerBounds.height)
                Orientation.HORIZONTAL -> Point(x = child.outerBounds.width)
            }
        }
        return viewPos
    }

    override val usedHeight
        get() = when (orientation) {
            Orientation.HORIZONTAL -> super.usedHeight
            Orientation.VERTICAL ->
                // We can't use FILL-based views in this calculation,
                // otherwise it runs into an infinite loop
                children
                    .filter { it.layoutParams.verticalSizing != ViewSizing.FILL }
                    .sumOf { it.outerBounds.height }
        }

    override val usedWidth
        get() = when (orientation) {
            Orientation.HORIZONTAL ->
                // We can't use FILL-based views in this calculation,
                // otherwise it runs into an infinite loop
                children
                    .filter { it.layoutParams.horizontalSizing != ViewSizing.FILL }
                    .sumOf { it.outerBounds.width }
            Orientation.VERTICAL -> super.usedWidth
        }

    override val remainingHeight: DP
        get() = when (orientation) {
            Orientation.VERTICAL -> innerHeight - usedHeight
            Orientation.HORIZONTAL -> innerHeight
        }

    override val remainingWidth: DP
        get() = when (orientation) {
            Orientation.VERTICAL -> innerWidth
            Orientation.HORIZONTAL -> innerWidth - usedWidth
        }

    override val height
        get() =
            when (layoutParams.verticalSizing) {
                ViewSizing.AUTO -> when (orientation) {
                    Orientation.VERTICAL -> (children
                        .filter { it.layoutParams.verticalSizing == ViewSizing.AUTO }
                        .sumOf { it.outerBounds.height }) + layoutParams.totalVerticalPadding
                    else -> super.height
                }
                else -> super.height
            }

    override val width
        get() =
            when (layoutParams.horizontalSizing) {
                ViewSizing.AUTO -> when (orientation) {
                    Orientation.HORIZONTAL -> (children
                        .filter { it.layoutParams.horizontalSizing == ViewSizing.AUTO }
                        .sumOf { it.outerBounds.width }) + layoutParams.totalHorizontalPadding
                    else -> super.width
                }
                else -> super.width
            }

    enum class Orientation {
        VERTICAL, HORIZONTAL
    }

}