package wtf.uuh.mc.roasty.client.ui.animations

import kotlin.math.sin

object SineAnimator : Animator() {

    override fun transformFactor(linearFactor: Float): Float {
        val angle = linearFactor * 90f
        return sin(Math.toRadians(angle.toDouble()).toFloat())
    }

}