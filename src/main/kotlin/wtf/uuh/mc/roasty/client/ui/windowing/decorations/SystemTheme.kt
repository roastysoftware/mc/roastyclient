package wtf.uuh.mc.roasty.client.ui.windowing.decorations

import wtf.uuh.mc.roasty.client.ui.color.ColorComponents

abstract class SystemTheme {

    abstract val primary: ColorComponents
    abstract val secondary: ColorComponents
    abstract val accent: ColorComponents

    open val accentDarken1 get() = accent.copy().darken(.2f)
    open val accentDarken2 get() = accent.copy().darken(.3f)
    open val accentLighten1 get() = accent.copy().lighten(.2f)
    open val accentLighten2 get() = accent.copy().lighten(.3f)

}