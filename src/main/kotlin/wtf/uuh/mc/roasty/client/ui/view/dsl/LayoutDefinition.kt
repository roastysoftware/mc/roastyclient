package wtf.uuh.mc.roasty.client.ui.view.dsl

import wtf.uuh.mc.roasty.client.ui.layout.LayoutParams
import wtf.uuh.mc.roasty.client.ui.view.View
import wtf.uuh.mc.roasty.client.ui.view.ViewGroup

class LayoutDefinition(val view: ViewGroup) {

    inline infix fun parameters(paramDef: LayoutParams.() -> Unit) {
        this.view.layoutParams.apply(paramDef)
    }

    infix fun addView(view: View) {
        this.view.addView(view)
    }

}