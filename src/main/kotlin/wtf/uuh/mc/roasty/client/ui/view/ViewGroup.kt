package wtf.uuh.mc.roasty.client.ui.view

import wtf.uuh.mc.roasty.client.graphics.opengl.GL.scissor
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.geometry.Rectangle
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.layout.ViewSizing
import wtf.uuh.mc.roasty.client.ui.render.DrawContext
import wtf.uuh.mc.roasty.client.ui.view.dsl.LayoutDefinition
import wtf.uuh.mc.roasty.client.ui.windowing.Window
import java.util.*

const val DEBUG_VISUAL = false

/**
 * A ViewGroup without any layout logic
 * is a view that can contain subviews, each
 * being overlaid on top of each other.
 * The total width and height is the same as the
 * width and height of the largest view, respectively.
 */
abstract class ViewGroup() : View() {
    private val views = ArrayList<View>()

    constructor(init: ViewGroup.() -> Unit) : this() {
        run(init)
    }

    open infix fun addView(view: View) {
        if (!views.contains(view)) {
            view.parent = this
            window?.let { view.onAttachToWindow(it) }
            views.add(view)
        }
    }

    open infix fun removeView(view: View) {
        views.remove(view)
        view.parent = null
        view.window?.run { view.onDetachFromWindow() }
    }

    operator fun invoke(childrenStructure: ViewGroup.() -> Unit) = apply(childrenStructure)

    override fun onAttachToWindow(window: Window) {
        super.onAttachToWindow(window)
        views.forEach { it.onAttachToWindow(window) }
    }

    override fun onDetachFromWindow() {
        super.onDetachFromWindow()
        views.forEach { it.onDetachFromWindow() }
    }

    override fun draw(context: DrawContext) {
        if (bounds == null || visibleArea == null) {
            return
        }
        scissor(pos, size) {
            views
                .filter { it.bounds != null && visibleArea!! intersectsWith it.bounds!! }
                .forEach { it.draw(context) }
        }
        if (DEBUG_VISUAL) {
            drawDebugOverlay(context)
            views
                .filter { it.bounds != null && visibleArea!! intersectsWith it.bounds!! }
                .filter { it !is ViewGroup }
                .forEach { it.drawDebugOverlay(context) }
        }
    }

    override val height
        get() =
            when (layoutParams.verticalSizing) {
                ViewSizing.AUTO -> (views
                    .filter { it.layoutParams.verticalSizing == ViewSizing.AUTO }
                    .maxOfOrNull { it.outerBounds.height } ?: 0.dp) + layoutParams.totalVerticalPadding
                ViewSizing.FILL_PARENT -> parent?.innerHeight
                ViewSizing.FILL -> parent?.remainingHeight
            } ?: 0.dp

    override val width
        get() =
            when (layoutParams.horizontalSizing) {
                ViewSizing.AUTO -> (views
                    .filter { it.layoutParams.horizontalSizing == ViewSizing.AUTO }
                    .maxOfOrNull { it.outerBounds.width } ?: 0.dp) + layoutParams.totalHorizontalPadding
                ViewSizing.FILL_PARENT -> parent?.innerWidth
                ViewSizing.FILL -> parent?.remainingWidth
            } ?: 0.dp

    val children: List<View>
        get() = Collections.unmodifiableList(views)

    val childrenCount: Int
        get() = views.size

    inline val innerOffset get() =
        Point(layoutParams.leftPadding, layoutParams.topPadding) + additionalInnerOffset

    inline val innerZeroPos
        get() = pos + innerOffset

    open val additionalInnerOffset get() = Point.zero

    open fun getViewPos(view: View): Point {
        require(views.contains(view)) { "Specified view is not child of this view" }
        return innerZeroPos
    }

    open val innerHeight: DP
        get() = height - layoutParams.totalVerticalPadding

    open val innerWidth: DP
        get() = width - layoutParams.totalHorizontalPadding


    open val remainingHeight: DP
        get() = innerHeight

    open val remainingWidth: DP
        get() = innerWidth

    open val usedHeight: DP
        get() = children.maxOfOrNull { it.outerBounds.height } ?: 0.dp

    open val usedWidth: DP
        get() = children.maxOfOrNull { it.outerBounds.width } ?: 0.dp

    infix fun layout(layoutDef: LayoutDefinition.() -> Unit) {
        LayoutDefinition(this).apply(layoutDef)
    }

    override fun onScalingChanged() {
        children.forEach { it.onScalingChangedInternal() }
    }

    fun invalidateRecursively() {
        invalidate()
        children.forEach {
            if (it is ViewGroup) {
                it.invalidateRecursively()
            } else {
                it.invalidate()
            }
        }
    }

}
