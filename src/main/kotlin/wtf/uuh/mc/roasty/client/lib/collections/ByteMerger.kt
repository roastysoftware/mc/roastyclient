package wtf.uuh.mc.roasty.client.lib.collections

import java.util.*

object ByteMerger {
    fun mergeBytes(vararg data: ByteArray): ByteArray {
        val totalSize = Arrays.stream(data).map { bytes: ByteArray -> bytes.size }
            .reduce{ a, b -> a + b }.orElse(0)
        val merged = ByteArray(totalSize)
        var cursor = 0
        for (bytes in data) {
            System.arraycopy(bytes, 0, merged, cursor, bytes.size)
            cursor += bytes.size
        }
        return merged
    }
}