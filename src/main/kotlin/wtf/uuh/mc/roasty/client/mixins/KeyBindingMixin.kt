package wtf.uuh.mc.roasty.client.mixins

import net.minecraft.client.option.KeyBinding
import net.minecraft.client.util.InputUtil
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo
import wtf.uuh.mc.roasty.client.ui.input.KeyboardInputDispatcher

@Mixin(KeyBinding::class)
class KeyBindingMixin {

    private companion object {
        @Inject(
            at = [At(value = "TAIL")],
            method = ["onKeyPressed(Lnet/minecraft/client/util/InputUtil\$Key;)V"]
        )
        @JvmStatic
        private fun onKeyPressed(key: InputUtil.Key, ci: CallbackInfo?) {
            KeyboardInputDispatcher.onKeyPressed(key)
        }
    }

}