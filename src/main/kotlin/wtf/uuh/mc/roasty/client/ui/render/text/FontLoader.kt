package wtf.uuh.mc.roasty.client.ui.render.text

import wtf.uuh.mc.roasty.client.resources.ResourcePath
import wtf.uuh.mc.roasty.client.resources.stream
import java.awt.Font

object FontLoader {

    private val loadedFonts by lazy { HashMap<ResourcePath, Font>() }
    private val defaultFont by lazy { font(FontRepository.ROBOTO) }
    val default get() = defaultFont
    val main get() = defaultFont

    fun font(fontPath: ResourcePath): Font {
        if (loadedFonts.containsKey(fontPath)) {
            return loadedFonts[fontPath]!!
        }
        val font = Font.createFont(Font.TRUETYPE_FONT, fontPath.stream())
        loadedFonts[fontPath] = font
        return font
    }

}