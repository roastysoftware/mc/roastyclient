package wtf.uuh.mc.roasty.client.ui.geometry

import wtf.uuh.mc.roasty.client.providers.MC
import wtf.uuh.mc.roasty.client.ui.extensions.guiScale
import kotlin.math.abs
import kotlin.math.ceil
import kotlin.reflect.KProperty

/**
 * Density-based Pixels (automatic scaling)
 */
class DP(val dp: Double = 0.0) : Comparable<DP>, Number() {

    companion object {
        fun fromAbsolute(d: Double) = DP(d, true)
        fun fromAbsolute(f: Float) = DP(f, true)
        fun fromAbsolute(i: Int) = DP(i, true)

        val zero = DP(0)
        val one = DP(1)
        val two = DP(2)
        val max = Double.MAX_VALUE.dp
        val min = Double.MIN_VALUE.dp
    }

    inline val absolute
        get() = dp * MC.guiScale.toDouble()
    inline val absInt: Int
        get() = ceil(absolute).toInt()
    inline val absFloat: Float
        get() = absolute.toFloat()


    inline val dpInt: Int
        get() = ceil(dp).toInt()
    inline val dpFloat: Float
        get() = dp.toFloat()

    constructor(value: Int, isAbsolute: Boolean = false) : this(value.toDouble(), isAbsolute)
    constructor(value: Float, isAbsolute: Boolean = false) : this(value.toDouble(), isAbsolute)
    constructor(value: Double, isAbsolute: Boolean) :
            this(if (isAbsolute) value / MC.guiScale.toDouble() else value)

    operator fun plus(other: DP) = DP(dp + other.dp)
    operator fun minus(other: DP) = DP(dp - other.dp)
    operator fun times(other: DP) = DP(dp * other.dp)
    operator fun div(other: DP) = DP(dp / other.dp)
    operator fun rem(other: DP) = DP(dp % other.dp)
    operator fun plus(other: Double) = DP(dp + other)
    operator fun minus(other: Double) = DP(dp - other)
    operator fun times(other: Double) = DP(dp * other)
    operator fun div(other: Double) = DP(dp / other)
    operator fun rem(other: Double) = DP(dp % other)

    operator fun unaryPlus() = DP(+dp)
    operator fun unaryMinus() = DP(-dp)

    override operator fun compareTo(other: DP): Int = dp.compareTo(other.dp)

    override fun equals(other: Any?) = other is DP && dp == other.dp
    override fun hashCode(): Int = dp.toFloat().toRawBits()

    override fun toByte() = dpInt.toByte()
    override fun toChar() = dp.toChar()
    override fun toDouble() = dp
    override fun toFloat() = dpFloat
    override fun toInt() = dpInt
    override fun toLong() = dp.toLong()
    override fun toShort() = dpInt.toShort()

    fun copy() = dp.dp

    override fun toString(): String {
        return "DP{dp:$dp,abs:$absolute}"
    }
}

inline val Int.dp get() =
    when (this) {
        0 -> DP.zero
        1 -> DP.one
        2 -> DP.two
        else -> DP(this)
    }

inline val Double.dp get() =
    when (this) {
        0.0 -> DP.zero
        1.0 -> DP.one
        2.0 -> DP.two
        else -> DP(this)
    }

inline val Float.dp get() =
    when (this) {
        0f -> DP.zero
        1f -> DP.one
        2f -> DP.two
        else -> DP(this)
    }

val Int.px get() = DP.fromAbsolute(this)
val Double.px get() = DP.fromAbsolute(this)
val Float.px get() = DP.fromAbsolute(this)

inline fun <T> Iterable<T>.sumOf(selector: (T) -> DP): DP {
    var sum = DP.zero
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

fun abs(dp: DP): DP {
    return DP(abs(dp.dp))
}