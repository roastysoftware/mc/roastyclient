package wtf.uuh.mc.roasty.client.ui.windowing.theming

import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.windowing.decorations.WindowManagerDecoration

class DefaultWindowManagerDecoration : WindowManagerDecoration() {
    override val backgroundColor: ColorComponents
        get() = ColorComponents.transparent
}