package wtf.uuh.mc.roasty.client.ui.animations.specific

import wtf.uuh.mc.roasty.client.ui.animations.Animatable
import wtf.uuh.mc.roasty.client.ui.animations.AnimationLike
import wtf.uuh.mc.roasty.client.ui.animations.OnAnimationEndListener
import wtf.uuh.mc.roasty.client.ui.animations.animation
import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.view.Invalidatable
import java.util.*

class ColorAnimation(val view: Invalidatable) : Animatable, AnimationLike<ColorAnimation> {

    private var target: ColorComponents = ColorComponents.transparent
    private var from: ColorComponents = ColorComponents.transparent
    private var to: ColorComponents = ColorComponents.transparent
    private val animation = animation().apply { addOnAnimationEndListener { this@ColorAnimation.onAnimationEnd() } }
    private val animationEndListeners = LinkedList<OnAnimationEndListener>()

    override fun onAnimationFrame(factor: Float) {
        target.applyColorForFrame(from, to, factor)
        view.invalidate()
    }

    private fun ColorComponents.applyColorForFrame(from: ColorComponents, to: ColorComponents, factor: Float) {
        val deltaRed = to.red - from.red
        val deltaGreen = to.green - from.green
        val deltaBlue = to.blue - from.blue
        red = from.red + deltaRed * factor
        green = from.green + deltaGreen * factor
        blue = from.blue + deltaBlue * factor
    }

    fun color(target: ColorComponents, from: ColorComponents, to: ColorComponents) = apply {
        this.target = target
        this.from = from
        this.to = to
    }

    override fun animate() = apply {
        animation.animate()
    }

    override fun reverse() = apply {
        animation.reverse()
    }

    override fun onAnimationEnd() {
        animationEndListeners.forEach { it() }
    }

    override fun addOnAnimationEndListener(listener: OnAnimationEndListener) {
        animationEndListeners += listener
    }
}