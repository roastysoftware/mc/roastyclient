package wtf.uuh.mc.roasty.client.ui.windowing.decorations

import wtf.uuh.mc.roasty.client.ui.color.ColorComponents

abstract class WindowManagerDecoration {
    abstract val backgroundColor: ColorComponents
}