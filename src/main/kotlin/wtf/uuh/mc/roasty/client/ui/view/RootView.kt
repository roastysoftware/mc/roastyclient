package wtf.uuh.mc.roasty.client.ui.view

import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.input.MouseButton
import wtf.uuh.mc.roasty.client.ui.layout.ViewSizing.FILL_PARENT
import wtf.uuh.mc.roasty.client.ui.windowing.Window

class RootView(windowToAttachTo: Window) : ViewGroup() {

    init {
        window = windowToAttachTo
        parent = windowToAttachTo.viewGroupProxy

        layout {
            parameters {
                horizontalSizing = FILL_PARENT
                verticalSizing = FILL_PARENT
            }
        }
    }

    override infix fun addView(view: View) {
        if (childrenCount > 0) {
            throw RuntimeException("RootView can only contain one view")
        }
        super.addView(view)
        super.onAttachToWindow(window!!)
    }

    override fun removeView(view: View) {
        if (childrenCount == 0) {
            return
        }
        super.removeView(view)
        super.onDetachFromWindow()
    }

    fun removeView() {
        if (childrenCount == 1) {
            removeView(children[0])
        }
    }

    override fun onMouseClickStateChange(
        mousePosition: Point,
        button: MouseButton,
        isClicked: Boolean
    ) {
        if (isClicked) {
            window!!.moveToFront()
        }
    }

    override val pos: Point
        get() = window!!.innerZeroPos

}