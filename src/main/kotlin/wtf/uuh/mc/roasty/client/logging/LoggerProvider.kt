package wtf.uuh.mc.roasty.client.logging

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import wtf.uuh.mc.roasty.client.modInfo;

private val loggerCache = hashMapOf<String, Logger>()

val logger: Logger
    get() {
        val name = modInfo.id + "|"
        return loggerCache.getOrElse(name) {
            val logger = LogManager.getFormatterLogger(name)
            loggerCache[name] = logger
            logger
        }
    }