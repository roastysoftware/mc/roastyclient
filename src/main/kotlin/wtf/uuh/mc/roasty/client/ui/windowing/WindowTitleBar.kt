package wtf.uuh.mc.roasty.client.ui.windowing

import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.geometry.Rectangle
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.input.MouseButton
import wtf.uuh.mc.roasty.client.ui.layout.ViewSizing.FILL_PARENT
import wtf.uuh.mc.roasty.client.ui.render.DrawContext
import wtf.uuh.mc.roasty.client.ui.render.RenderImpl
import wtf.uuh.mc.roasty.client.ui.view.ViewGroup
import wtf.uuh.mc.roasty.client.ui.widgets.TextView

class WindowTitleBar : ViewGroup() {

    private val title = TextView()
    private var isDraggingWindow = false
    private var dragDelta = Point.zero

    fun onDecorationChange() {
        val decor = window!!.decoration
        title.color = decor.titleBarTextColor
        layout {
            parameters {
                margin(decor.titleBarParams)
                padding(decor.titleBarParams)
            }
        }
    }

    fun onTitleChange() {
        title.text = window!!.title
    }

    override fun draw(context: DrawContext) {
        RenderImpl.runTranslated(context.matrixStack, pos) {
            RenderImpl.drawRectangle(
                context.matrixStack,
                Rectangle(width, height),
                window!!.decoration.titleBarBackgroundColor
            )
        }

        super.draw(context)
    }

    override fun onMouseClickStateChange(
        mousePosition: Point,
        button: MouseButton,
        isClicked: Boolean
    ) {
        if (button == MouseButton.LEFT) {
            if (isDraggingWindow && isClicked) {
                return
            }
            isDraggingWindow = isClicked
            if (isClicked) {
                dragDelta = mousePosition - pos
            }
        }
    }

    override fun onMouseMove(mousePosition: Point) {
        if (isDraggingWindow) {
            val delta = mousePosition - pos
            val moveDelta = delta - dragDelta
            val wm = window!!.windowManager
            window!!.overwritePos(
                (window!!.pos + moveDelta)
                    .clamp(
                        0.dp, 0.dp,
                        wm.width - window!!.width, wm.height - window!!.height
                    )
            )
        }
    }

    init {
        layout {
            parameters {
                horizontalSizing = FILL_PARENT
            }
            addView(title)
        }
    }
}