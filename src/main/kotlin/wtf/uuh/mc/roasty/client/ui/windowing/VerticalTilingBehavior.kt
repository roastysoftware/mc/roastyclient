package wtf.uuh.mc.roasty.client.ui.windowing

import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.geometry.dp

class VerticalTilingBehavior(windowManager: WindowManager) : WindowManagerBehavior(windowManager) {
    private var lastWindow: Window? = null

    override fun windowPlacementOrientation(): WindowPlacement {
        return WindowPlacement.VERTICAL_BY_DEFAULT
    }

    override fun nextWindowPosition(window: Window): Point {
        var nextPos: Point
        if (lastWindow != null) {
            val lastPos = lastWindow!!.pos
            nextPos = Point(lastPos.x, lastWindow!!.endY + WINDOW_GAP_SIZE)
            if (nextPos.y + window.height > windowManager.height) {
                nextPos = Point(lastWindow!!.endY + WINDOW_GAP_SIZE, INITIAL_POSITION.y)
            }
        } else {
            nextPos = INITIAL_POSITION.copy()
        }
        lastWindow = window
        return nextPos
    }

    override fun resetWindowPosition() {
        lastWindow = null
    }

    companion object {
        val WINDOW_GAP_SIZE = 16.dp
        private val INITIAL_POSITION = Point(8.dp, 8.dp)
    }
}