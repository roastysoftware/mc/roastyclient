package wtf.uuh.mc.roasty.client.ui.color

import java.awt.Color
import kotlin.math.max
import kotlin.math.min

class ColorComponents() {

    companion object {
        fun fromAwt(color: Color): ColorComponents {
            return ColorComponents(color.red, color.green, color.blue, color.alpha)
        }

        inline val black get() = ColorComponents(alpha = 1f)
        inline val white get() = ColorComponents(1f, 1f, 1f)
        inline val transparent get() = ColorComponents(alpha = 0f)
    }

    var red = 0f
    var green = 0f
    var blue = 0f
    var alpha = 0f

    var hue
        get() = rgbToHsv().first
        set(hue) {
            val rgb = hsvToRgb(hue.coerceAtMost(1f), saturation, value)
            red = rgb.first
            green = rgb.second
            blue = rgb.third
        }

    var saturation
        get() = rgbToHsv().second
        set(saturation) {
            val rgb = hsvToRgb(hue, saturation.coerceAtMost(1f), value)
            red = rgb.first
            green = rgb.second
            blue = rgb.third
        }

    var value
        get() = rgbToHsv().third
        set(value) {
            val rgb = hsvToRgb(hue, saturation, value.coerceAtMost(1f))
            red = rgb.first
            green = rgb.second
            blue = rgb.third
        }

    inline val packedColor get() = Coloring.toColor(this)

    inline val awtColor get() = Color(red, green, blue, alpha)

    constructor(red: Float = 0f, green: Float = 0f, blue: Float = 0f, alpha: Float = 1f) : this() {
        when {
            red !in 0f..1f -> ::red
            green !in 0f..1f -> ::green
            blue !in 0f..1f -> ::blue
            alpha !in 0f..1f -> ::alpha
            else -> null
        }?.let { throw IllegalArgumentException("${it.name} must be >= 0f and <= 1f") }
        this.red = red
        this.green = green
        this.blue = blue
        this.alpha = alpha
    }

    constructor(red: Int = 0, green: Int = 0, blue: Int = 0, alpha: Int = 255) :
            this(red / 255f, green / 255f, blue / 255f, alpha / 255f) {
        when {
            red !in 0..255 -> ::red
            green !in 0..255 -> ::green
            blue !in 0..255 -> ::blue
            alpha !in 0..255 -> ::alpha
            else -> null
        }?.let { throw IllegalArgumentException("${it.name} must be >= 0 and <= 255") }
    }

    constructor(hex: String) : this() {
        require(hex.startsWith("#")) { "Color hex string must start with #" }
        require(hex.length == 4 || hex.length == 7 || hex.length == 9)
            { "Color hex string must be like #fff, #ffffff or #ffffffff" }
        val rawHex = hex.substring(1).toLowerCase()
        require(rawHex.all { it in '0'..'9' || it in 'a'..'f' }) { "Color hex string contains non-hex characters" }
        val multiplier = if (rawHex.length >= 6) 2 else 1
        var offs = 0
        if (rawHex.length == 8) {
            offs += 2
            alpha = Integer.parseInt(rawHex.substring(0, 2), 16).toFloat() / 255f
        } else {
            alpha = 1.0f
        }
        red = Integer.parseInt(rawHex.substring(offs, offs + multiplier), 16).toFloat() / 255f
        green = Integer.parseInt(rawHex.substring(offs + multiplier, offs + 2 * multiplier), 16).toFloat() / 255f
        blue = Integer.parseInt(rawHex.substring(offs + 2 * multiplier, offs + 3 * multiplier), 16).toFloat() / 255f
    }

    private fun rgbToHsv(): Triple<Float, Float, Float> {
        val min = min(min(red, green), blue)
        val max = max(max(red, green), blue)
        val delta = max - min
        var hue = max
        var saturation = max
        if (delta == 0f) {
            hue = 0f
            saturation = 0f
        } else {
            saturation = delta / max
            val delR = ((max - red) / 6f + delta / 2f) / delta
            val delG = ((max - green) / 6f + delta / 2f) / delta
            val delB = ((max - blue) / 6f + delta / 2f) / delta
            when {
                red == max -> hue = delB - delG
                green == max -> hue = 1 / 3f + delR - delB
                blue == max -> hue = 2 / 3f + delG - delR
            }
            if (hue < 0) hue += 1f
            if (hue > 1) hue -= 1f
        }
        return Triple(hue, saturation, max)
    }

    private fun hsvToRgb(hue: Float, saturation: Float, value: Float): Triple<Float, Float, Float> {
        val h = (hue * 6).toInt()
        val f = hue * 6 - h
        val p = value * (1 - saturation)
        val q = value * (1 - f * saturation)
        val t = value * (1 - (1 - f) * saturation)
        return when (h) {
            0 -> Triple(value, t, p)
            1 -> Triple(q, value, p)
            2 -> Triple(p, value, t)
            3 -> Triple(p, q, value)
            4 -> Triple(t, p, value)
            5 -> Triple(value, p, q)
            else -> throw RuntimeException("Error while converting from HSV($hue, $saturation, $value) to RGB")
        }
    }

    fun lighten(by: Float) = apply {
        value = 1f + by
    }

    fun darken(by: Float) = apply {
        value /= 1f + by
    }

    fun saturate(by: Float) = apply {
        saturation *= 1f + by
    }

    fun desaturate(by: Float) = apply {
        saturation /= 1f + by
    }

    override fun toString() = "Color(r:$red, g:$green, b:$blue, a: $alpha, h:$hue, s:$saturation, v:$value)"
    fun copy() = ColorComponents(red, green, blue, alpha)
}