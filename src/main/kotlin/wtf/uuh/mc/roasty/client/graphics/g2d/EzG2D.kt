package wtf.uuh.mc.roasty.client.graphics.g2d

import java.awt.Graphics2D
import java.awt.image.BufferedImage

fun newGraphics2D(width: Int, height: Int): Pair<BufferedImage, Graphics2D> {
    val img = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
    return Pair(img, img.createGraphics())
}