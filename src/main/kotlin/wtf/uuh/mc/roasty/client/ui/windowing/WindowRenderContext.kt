package wtf.uuh.mc.roasty.client.ui.windowing

import net.minecraft.client.util.math.MatrixStack

data class WindowRenderContext(
    val window: Window,
    val matrixStack: MatrixStack
) 