package wtf.uuh.mc.roasty.client.mixins

import net.minecraft.client.Mouse
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.Shadow
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo
import wtf.uuh.mc.roasty.client.providers.MC
import wtf.uuh.mc.roasty.client.ui.input.MouseInputDispatcher

@Mixin(Mouse::class)
class MouseMixin {
    @Shadow
    private val x = 0.0

    @Shadow
    private val y = 0.0

    private val window = MC.window

    @Inject(at = [At(value = "TAIL")], method = ["onCursorPos(JDD)V"])
    fun onCursorPos(glfwWindow: Long, x: Double, y: Double, ci: CallbackInfo) {
        val scaledX = x * window.scaledWidth / window.width.toDouble()
        val scaledY = y * window.scaledHeight / window.height.toDouble()
        MouseInputDispatcher.onMousePosChange(scaledX, scaledY)
    }

    @Inject(at = [At(value = "TAIL")], method = ["onMouseButton(JIII)V"])
    fun onMouseButton(window: Long, button: Int, action: Int, mods: Int, ci: CallbackInfo) {
        MouseInputDispatcher.onMouseButtonStateChange(button, action, mods)
    }

    @Inject(
        at = [At(
            value = "INVOKE",
            target = "net/minecraft/client/gui/screen/Screen.mouseScrolled(DDD)Z")
        ],
        method = ["onMouseScroll(JDD)V"]
    )
    fun onMouseScroll(window: Long, horizontal: Double, vertical: Double, ci: CallbackInfo) {
        MouseInputDispatcher.onMouseScroll(horizontal, vertical)
    }

}