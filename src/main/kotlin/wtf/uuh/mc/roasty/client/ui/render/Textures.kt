package wtf.uuh.mc.roasty.client.ui.render

import org.lwjgl.opengl.GL12.*
import wtf.uuh.mc.roasty.client.logging.logger

class TextureContext {
    var textureId: Int = -1
        private set
    var bound: Boolean = false

    fun arm() {
        if (textureId == -1) {
            textureId = glGenTextures()
        }
    }

    fun bind() {
        arm()
        if (!bound) {
            glBindTexture(GL_TEXTURE_2D, textureId)
            bound = true
        }
    }

    inline fun start(fn: TextureContext.() -> Unit) {
        bind()
        run(fn)
    }

    inline fun use(fn: TextureContext.() -> Unit) {
        bind()
        run(fn)
        unbind()
    }

    fun unbind() {
        if (bound) {
            glBindTexture(GL_TEXTURE_2D, 0)
            bound = false
        }
    }

    fun end(fn: TextureContext.() -> Unit) {
        if (!bound) {
            throw IllegalStateException("Texture not bound, can't end()")
        }
        run(fn)
        unbind()
    }

    override fun toString() = "TextureContext(textureId=$textureId,bound=$bound)"
    fun release() {
        if (textureId != -1) {
            unbind()
            Textures.release(textureId)
            textureId = -1
        }
    }

}

object Textures {

    inline fun texture(fn: TextureContext.() -> Unit): TextureContext {
        val context = TextureContext()
        context.start(fn)
        return context
    }

    inline fun bindTexture(texture: Int?, target: Int = GL_TEXTURE_2D, fn: (texture: Int) -> Unit) {
        val actualTexture =
            if (texture != null && !glIsTexture(texture) || texture == null) {
                texture?.let {
                    logger.warn("Texture $texture does not exist, creating new one. This may cause artifacts.")
                }
                glGenTextures()
            } else {
                texture
            }
        glBindTexture(target, actualTexture)
        fn(actualTexture)
        glBindTexture(target, 0)
    }

    inline fun bindTexture(fn: (texture: Int) -> Unit) {
        bindTexture(null, GL_TEXTURE_2D, fn)
    }

    fun release(texture: Int, target: Int = GL_TEXTURE_2D) {
        glDeleteTextures(texture)
    }

}