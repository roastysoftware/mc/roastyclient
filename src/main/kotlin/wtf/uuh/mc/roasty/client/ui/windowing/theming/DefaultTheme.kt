package wtf.uuh.mc.roasty.client.ui.windowing.theming

import wtf.uuh.mc.roasty.client.ui.color.*
import wtf.uuh.mc.roasty.client.ui.windowing.decorations.SystemTheme

class DefaultTheme : SystemTheme() {
    override val primary: ColorComponents = gray900
    override val secondary: ColorComponents = gray800
    override val accent: ColorComponents = blue600
    override val accentDarken1: ColorComponents = blue700
    override val accentDarken2: ColorComponents = blue800
}