package wtf.uuh.mc.roasty.client.ui.widgets

import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.render.canvas.Canvas
import wtf.uuh.mc.roasty.client.ui.render.canvas.textHeight
import wtf.uuh.mc.roasty.client.ui.render.canvas.textWidth
import wtf.uuh.mc.roasty.client.ui.render.text.TextSizing
import wtf.uuh.mc.roasty.client.ui.view.CanvasBasedView

class TextView() : CanvasBasedView() {

    constructor(init: TextView.() -> Unit) : this() { run(init) }

    var text: String = ""
        set(value) {
            field = value
            invalidate()
        }

    var textSize: DP = 8.dp
        set(value) {
            field = value
            invalidate()
        }

    var color: ColorComponents = ColorComponents.white
        set(value) {
            field = value
            invalidate()
        }

    override fun paint(canvas: Canvas) = canvas.run {
        text(text, textSize, this@TextView.color)
    }

    override val height: DP
        get() = textHeight(text, textSize)

    override val width: DP
        get() = textWidth(text, textSize)

}