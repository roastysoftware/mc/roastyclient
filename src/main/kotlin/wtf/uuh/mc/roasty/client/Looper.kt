package wtf.uuh.mc.roasty.client

import wtf.uuh.mc.roasty.client.logging.logger
import java.util.concurrent.LinkedBlockingQueue

private const val DEBUG = false

val mainLoop by lazy { Looper() }
val inGameHudLoop by lazy { Looper() }

typealias Runnable = QueueEntryControl.() -> Unit

class QueueEntryControl(private val anyIt: Any, val onCancel: (Any) -> Unit) {

    var shouldRun = true
        private set
        get() = !canceled && field
    var canceled = false
        private set
    var rethrowExceptions = false
        private set

    fun cancel() {
        if (DEBUG) {
            logger.info("Requested cancellation of: $anyIt (already canceled: $canceled)")
        }
        if (!canceled) {
            stop()
            canceled = true
            onCancel(anyIt)
            if (DEBUG) {
                logger.info("Cancellation of $anyIt succeeded")
            }
        }
    }

    fun stop() {
        shouldRun = false
    }

    fun resume() {
        if (!canceled) {
            shouldRun = true
        }
    }

    fun enableRethrowingExceptions() {
        rethrowExceptions = true
    }
}

private class QueueEntry(
    val runnable: Runnable,
    val delay: Long = 0,
    val interval: Long? = null,
    val posted: Long = System.currentTimeMillis(),
    val onCancel: (QueueEntry) -> Unit
) {
    inline val effectiveInterval: Long?
        get() = interval?.coerceAtLeast(0)

    var control: QueueEntryControl = QueueEntryControl(this) { onCancel(it as QueueEntry) }

    fun run() = control.run(runnable)
}

class Looper {

    private val queue = LinkedBlockingQueue<QueueEntry>()

    fun post(
        interval: Long? = null, delay: Long? = null, control: QueueEntryControl? = null, runnable: Runnable
    ): QueueEntryControl {
        if (DEBUG) {
            logger.info("Posted runnable with interval $interval and delay $delay")
        }
        val entry = QueueEntry(runnable, delay ?: interval ?: 0, interval, onCancel = { cancel(it) })
        control?.let { entry.control = control }
        queue.offer(entry)
        return entry.control
    }

    private fun cancel(queueEntry: QueueEntry) {
        if (queueEntry.control.canceled) {
            queue -= queueEntry
        } else {
            queueEntry.control.cancel()
        }
    }

    private fun runQueueEntryNow(entry: QueueEntry, millisBeforeRun: Long) = entry.run {
        try {
            if (DEBUG) {
                logger.info("Running queue entry now. Interval $interval")
            }
            run()
        } catch (e: Exception) {
            if (control.rethrowExceptions) {
                throw RuntimeException(e)
            } else {
                logger.error("An error occurred during looper post, skipping", e)
            }
        }
        if (interval != null && !control.canceled) {
            post(interval, effectiveInterval!! - (System.currentTimeMillis() - millisBeforeRun), control, runnable)
        }
    }

    private fun runQueueEntry(entry: QueueEntry): Unit = entry.run {
        if (!control.shouldRun) {
            if (DEBUG) {
                logger.info("This entry should not be run and thus will be skipped. (Canceled: ${control.canceled}")
            }
            return
        }
        if (delay > 0) {
            val now = System.currentTimeMillis()
            if (posted + delay < now) {
                runQueueEntryNow(this, now)
            } else {
                // Requeue
                queue.offer(this)
            }
        } else {
            runQueueEntryNow(this, System.currentTimeMillis())
        }
    }

    fun runQueue() {
        var size = queue.size
        while (size-- > 0) {
            runQueueEntry(queue.take())
        }
    }

    operator fun invoke(fn: QueueEntryControl.() -> Unit) {
        post(runnable = fn)
    }

}