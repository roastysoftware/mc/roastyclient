package wtf.uuh.mc.roasty.client.ui.widgets

import wtf.uuh.mc.roasty.client.ui.animations.specific.ColorAnimation
import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.render.canvas.Canvas
import wtf.uuh.mc.roasty.client.ui.view.CanvasBasedViewGroup

class Button() : CanvasBasedViewGroup() {

    constructor(init: Button.() -> Unit) : this() {
        run(init)
    }

    private val textView = TextView()
    var text: String
        get() = textView.text
        set(value) {
            textView.text = value
            invalidate()
        }

    var textSize: DP
        get() = textView.textSize
        set(value) {
            textView.textSize = value
            invalidate()
        }

    private var textColor: ColorComponents by textView::color
    private var backgroundColor = normalBackgroundColor

    private val normalBackgroundColor get() = theme.accent.copy()
    private val hoveredBackgroundColor get() = theme.accentDarken2.copy()
    private val backgroundAnimation by lazy {
        ColorAnimation(this).color(backgroundColor, normalBackgroundColor, hoveredBackgroundColor)
    }


    private val hoveredTextColor get() = ColorComponents.white
    private val normalTextColor get() = ColorComponents.white
    private val textAnimation by lazy {
        ColorAnimation(this).color(textColor, normalTextColor, hoveredTextColor)
    }

    init {
        layout {
            parameters {
                padding(4.dp)
            }
            addView(textView)
        }
    }

    override fun paint(canvas: Canvas) = canvas.run {
        rectangle(width, height, this@Button.backgroundColor, 5.dp)
    }

    override fun onMouseEnter() {
        backgroundAnimation.animate()
        textAnimation.animate()
    }

    override fun onMouseLeave() {
        backgroundAnimation.reverse()
        textAnimation.reverse()
    }

}