package wtf.uuh.mc.roasty.client.exceptions

open class UnknownFormatException(message: String) : RuntimeException(message)
open class UnsupportedFormatException(message: String) : RuntimeException(message)