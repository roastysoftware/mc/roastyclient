package wtf.uuh.mc.roasty.client.ui.windowing

import com.mojang.blaze3d.systems.RenderSystem.assertOnRenderThread
import net.minecraft.client.gui.DrawableHelper
import net.minecraft.client.gui.screen.Screen
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.text.Text
import wtf.uuh.mc.roasty.client.logging.logger
import wtf.uuh.mc.roasty.client.providers.MC
import wtf.uuh.mc.roasty.client.ui.extensions.guiScale
import wtf.uuh.mc.roasty.client.ui.geometry.Rectangle
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.input.MouseInputDispatcher
import wtf.uuh.mc.roasty.client.ui.windowing.decorations.WindowManagerDecoration
import wtf.uuh.mc.roasty.client.ui.windowing.theming.DefaultWindowManagerDecoration
import java.util.function.Consumer

class WindowManager {
    val screen = object : Screen(Text.empty()) {
        override fun close() {
            this@WindowManager.onClose()
            super.close()
        }
    }

    private val windows = arrayListOf<Window>()
    private val windowListeners = arrayListOf<WindowListener>()
    private val decoration: WindowManagerDecoration = DefaultWindowManagerDecoration()
    private val behavior: WindowManagerBehavior = VerticalTilingBehavior(this)

    var usuallyVisible = true
    val isVisible
        get() = usuallyVisible && !isScreenClosed

    private var isScreenClosed = true
    private var wasClosedLastFrame = false
    private var shouldBeShowing = false

    inline val width get() = screen.width.dp
    inline val height get() = screen.height.dp
    private var lastSize = Rectangle(0.dp, 0.dp)
    private var lastScale = 0

    private inline val visibleWindows
        get() = windows
            .filter { it.isVisible }
            .sortedByDescending { it.zIndex }

    private inline val windowsToRender
        get() = when {
            isInvisible && !wasClosedLastFrame -> visibleWindows.filter { window -> window.isPinned }
            else -> visibleWindows
        }

    init {
        allInstances.add(this)
    }

    fun destroy() {
        allInstances.remove(this)
    }

    fun close(window: Window?) {
        windows.remove(window)
    }

    private fun notifyOnWindowClose(window: Window) {
        windowListeners.forEach(Consumer { listener: WindowListener ->
            listener.onClose(
                window
            )
        })
    }

    fun register(window: Window) {
        windows.add(window)
        window.zIndex = windows.size.toDouble()
    }

    private fun notifyOnWindowOpen(window: Window) {
        windowListeners.forEach(Consumer { listener: WindowListener ->
            listener.onOpen(
                window
            )
        })
    }

    fun addWindowListener(listener: WindowListener) {
        windowListeners.add(listener)
    }

    fun removeWindowListener(listener: WindowListener?) {
        windowListeners.remove(listener)
    }

    private fun onSizeChanged() {

    }

    private fun renderWindows(matrixStack: MatrixStack) {
        if (isVisible) {
            draw(matrixStack)
        }
        windowsToRender.forEach { it.render(matrixStack) }
    }

    private fun handlePossibleSizeChange() {
        if (lastSize.width != width || lastSize.height != height) {
            onSizeChanged()
            lastSize = Rectangle(width, height)
        }
    }

    private fun applyWindowPositions() {
        behavior.resetWindowPosition()
        windows.forEach { window ->
            window.pos = behavior.nextWindowPosition(window)
        }
    }

    private fun doShow() {
        MouseInputDispatcher.resetState()
        usuallyVisible = true
        isScreenClosed = false
        MC.setScreen(screen)
    }

    fun render(matrixStack: MatrixStack) {
        try {
            if (shouldBeShowing && isScreenClosed) {
                doShow()
            }
            assertOnRenderThread()
            handlePossibleScaleChange()
            applyWindowPositions()
            handlePossibleSizeChange()
            renderWindows(matrixStack)
        } catch (e: Exception) {
            logger.error("An exception occurred during render, result may be incomplete", e)
        }
    }

    private fun handlePossibleScaleChange() {
        if (lastScale == 0) {
            lastScale = MC.guiScale
            return
        }
        if (isVisible) {
            if (lastScale != MC.guiScale) {
                lastScale = MC.guiScale
                windowsToRender.forEach(Window::onScalingChanged)
            }
        }
    }

    private fun draw(matrixStack: MatrixStack?) {
        if (isVisible) {
            DrawableHelper.fill(matrixStack, 0, 0, width.absInt, height.absInt, decoration.backgroundColor.packedColor)
        }
    }

    inline val isInvisible: Boolean
        get() = !isVisible

    fun hide() {
        usuallyVisible = false
        onClose()
    }

    fun show() {
        shouldBeShowing = true
    }

    fun onClose() {
        isScreenClosed = true
        windows.filter { !it.isEffectivelyVisible }.forEach(Window::invalidate)
    }

    fun moveToFront(window: Window) {
        val currentZ = window.zIndex
        val newHighestIndex = windows.size.toDouble()
        window.zIndex = newHighestIndex
        windows
            .filter { it.zIndex >= currentZ }
            .forEach { moveBackward(it) }
    }

    fun moveToBack(window: Window) {
        windows.forEach { moveForward(it) }
        window.zIndex = 0.0
    }

    fun moveBackward(window: Window) {
        window.zIndex = window.zIndex - 1
    }

    fun moveForward(window: Window) {
        window.zIndex = window.zIndex + 1
    }

    companion object {
        private val allInstances = arrayListOf<WindowManager>()

        fun onRender(matrixStack: MatrixStack) {
            allInstances.forEach {
                it.render(matrixStack)
            }
        }
    }

}