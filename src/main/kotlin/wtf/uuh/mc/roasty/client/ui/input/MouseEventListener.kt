package wtf.uuh.mc.roasty.client.ui.input

import wtf.uuh.mc.roasty.client.ui.geometry.Box
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.Point

interface MouseEventListener {
    fun onMouseMove(mousePosition: Point) {}
    fun onMouseClickStateChange(
        mousePosition: Point,
        button: MouseButton,
        isClicked: Boolean
    ) {}

    fun onMouseClick(mousePosition: Point, button: MouseButton?) {}
    fun onMouseEnter() {}
    fun onMouseLeave() {}
    fun onMouseScroll(horizontal: DP, vertical: DP) {}

    val shouldListenToMouse: Boolean
        get() = true

    val isScrollReceiver: Boolean
        get() = false

    /**
     * Bounds inside of which events are triggered for.
     * @return bounds. null means no events.
     */
    val visibleArea: Box?
        get() = null

    val zIndex: Double
        get() = .0
}