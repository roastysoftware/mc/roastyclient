package wtf.uuh.mc.roasty.client.ui.windowing

import wtf.uuh.mc.roasty.client.ui.geometry.Rectangle
import wtf.uuh.mc.roasty.client.ui.render.RenderImpl
import wtf.uuh.mc.roasty.client.ui.render.RenderImpl.runTranslated
import wtf.uuh.mc.roasty.client.ui.render.Runner

object WindowRenderer {

    private fun atWindow(ctx: WindowRenderContext, runner: Runner) {
        runTranslated(ctx, ctx.window.pos.x, ctx.window.pos.y, runner)
    }

    fun render(ctx: WindowRenderContext) {
        atWindow(ctx) {
            RenderImpl.drawRectangle(
                ctx.matrixStack,
                Rectangle(ctx.window.width, ctx.window.height),
                ctx.window.decoration.backgroundColor
            )
        }
    }

}