package wtf.uuh.mc.roasty.client.ui.view

import wtf.uuh.mc.roasty.client.ui.drawable.Bitmap
import wtf.uuh.mc.roasty.client.ui.render.DrawContext
import wtf.uuh.mc.roasty.client.ui.render.TextureContext

abstract class CanvasBasedViewGroup : ViewGroup(), CanvasViewBase {

    override var cachedBitmap: Bitmap? = null
    override val isInvalidated
        get() = invalidated
    override var textureContext = TextureContext()

    override fun draw(context: DrawContext) {
        super<CanvasViewBase>.draw()
        super<ViewGroup>.draw(context)
    }

    override fun onHide() = super<CanvasViewBase>.onHide()
    override fun onShow() = super<CanvasViewBase>.onShow()
    override fun invalidate() = super<CanvasViewBase>.invalidate()

}