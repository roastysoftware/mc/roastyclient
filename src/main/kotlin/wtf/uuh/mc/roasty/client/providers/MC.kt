package wtf.uuh.mc.roasty.client.providers

import net.minecraft.client.MinecraftClient

val MC: MinecraftClient by lazy { MinecraftClient.getInstance() }
