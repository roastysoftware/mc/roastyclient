package wtf.uuh.mc.roasty.client.ui.render.text

import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.geometry.px
import java.awt.image.BufferedImage

object TextSizing {

    private val reusableBufferedImage = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)
    private const val textHeightFallbackString = "Ay|`_"

    fun textHeight(text: String = textHeightFallbackString, size: DP): DP {
        val g2d = reusableBufferedImage.createGraphics()
        g2d.font = FontLoader.font(FontRepository.ROBOTO).deriveFont(size.absFloat)
        val bounds = g2d.fontMetrics.getStringBounds(text, g2d)
        g2d.dispose()
        return bounds.height.px
    }

    fun textWidth(text: String, size: DP): DP {
        val g2d = reusableBufferedImage.createGraphics()
        g2d.font = FontLoader.font(FontRepository.ROBOTO).deriveFont(size.absFloat)
        val fm = g2d.fontMetrics
        g2d.dispose()
        return fm.stringWidth(text).px
    }

}