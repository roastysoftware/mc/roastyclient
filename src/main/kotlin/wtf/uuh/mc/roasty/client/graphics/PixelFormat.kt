package wtf.uuh.mc.roasty.client.graphics

enum class PixelFormat {
    RGBA_PACKED,
    BGRA_PACKED,
}