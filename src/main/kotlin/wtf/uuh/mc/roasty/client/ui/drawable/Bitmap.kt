package wtf.uuh.mc.roasty.client.ui.drawable

import wtf.uuh.mc.roasty.client.graphics.BitmapMetadata
import wtf.uuh.mc.roasty.client.graphics.PixelFormat
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.render.TextureContext

open class Bitmap(
    var image: IntArray?,
    var width: DP,
    var height: DP,
    var metadata: BitmapMetadata = BitmapMetadata(PixelFormat.BGRA_PACKED),

    var textureContext: TextureContext? = null
) {

    override fun equals(other: Any?): Boolean {
        if (other is Bitmap) {
            return this === other ||
                    textureContext != null && textureContext!!.textureId == other.textureContext?.textureId
        }
        return this === other
    }

    override fun hashCode(): Int {
        throw NotImplementedError("hashCode can't be used in here")
    }

    override fun toString(): String {
        var fullStr = "w:$width h:$height meta:$metadata tex:$textureContext\n\n"
        if (image != null) {
            for (y in 0 until height.absInt) {
                for (x in 0 until width.absInt) {
                    fullStr += if (image!![y * width.absInt + x] == 0) " " else "#"
                }
                fullStr += "\n"
            }
        } else {
            fullStr += "No image data available."
        }
        return fullStr
    }
}
