package wtf.uuh.mc.roasty.client.ui.animations

import wtf.uuh.mc.roasty.client.QueueEntryControl
import wtf.uuh.mc.roasty.client.inGameHudLoop
import wtf.uuh.mc.roasty.client.providers.MC
import java.util.*
import kotlin.math.ceil

typealias OnAnimationEndListener = () -> Unit

interface AnimationLike<A> {
    fun animate(): A
    fun reverse(): A
    fun onAnimationEnd() {}
    fun addOnAnimationEndListener(listener: OnAnimationEndListener)
}

class Animation(
    val animatable: Animatable,
    val duration: Int = DEFAULT_ANIMATION_DURATION,
    val animator: Animator = QuadAnimator,
) : AnimationLike<Animation> {

    private val factorHistory = ArrayList<Float>(ceil(duration / MC.lastFrameDuration).toInt())
    private var queueEntryControl: QueueEntryControl = animator.animate(this)
    private var animationRequested = true
    private var hasStartedAnimating = false
    private var hasDoneFullAnimation = false
    private var factorHistoryIndex = 0
    private val animationEndListeners = LinkedList<OnAnimationEndListener>()

    fun onAnimationFrameFromAnimator(factor: Float) {
        hasStartedAnimating = true
        animatable.onAnimationFrame(factor)
        factorHistory += factor
        factorHistoryIndex = factorHistory.size - 1
    }

    override fun onAnimationEnd() {
        hasDoneFullAnimation = true
        animationRequested = false
        hasStartedAnimating = false
        animatable.onAnimationEnd()
        animationEndListeners.forEach { it() }
    }

    override fun reverse() = apply {
        cancel()
        animationRequested = false
        if (factorHistoryIndex == -1) {
            return@apply
        }
        if (factorHistoryIndex == factorHistory.size) {
            factorHistoryIndex--
        }
        queueEntryControl = inGameHudLoop.post(0) {
            if (factorHistoryIndex <= 0) {
                animatable.onAnimationFrame(0f)
                if (!hasDoneFullAnimation) {
                    factorHistory.clear()
                }
                cancel()
                return@post
            }
            animatable.onAnimationFrame(factorHistory[factorHistoryIndex])
            factorHistoryIndex--
        }
    }

    override fun animate() = apply {
        if (animationRequested) {
            return@apply
        }
        cancel()
        if (hasDoneFullAnimation) {
            if (factorHistory.size == 0 || factorHistoryIndex >= factorHistory.size) {
                return@apply
            }
            queueEntryControl = inGameHudLoop.post(0) {
                if (factorHistoryIndex >= factorHistory.size) {
                    cancel()
                    return@post
                }
                animatable.onAnimationFrame(factorHistory[factorHistoryIndex])
                factorHistoryIndex++
            }
            return@apply
        }
        factorHistory.clear()
        queueEntryControl = animator.animate(this)
        animationRequested = true
    }

    fun cancel() = apply {
        queueEntryControl.cancel()
    }

    fun reset() {
        factorHistoryIndex = 0
    }

    override fun addOnAnimationEndListener(listener: OnAnimationEndListener) {
        animationEndListeners += listener
    }

}

fun <A : Animatable> A.animation(duration: Int = DEFAULT_ANIMATION_DURATION, animator: Animator = SineAnimator) =
    Animation(this, duration, animator)
