package wtf.uuh.mc.roasty.client.mixins

import com.mojang.authlib.GameProfile
import net.minecraft.client.gui.hud.InGameHud
import net.minecraft.client.util.Session
import net.minecraft.client.util.math.MatrixStack
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.Shadow
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable
import wtf.uuh.mc.roasty.client.inGameHudLoop
import wtf.uuh.mc.roasty.client.logging.logger
import wtf.uuh.mc.roasty.client.ui.windowing.WindowManager
import java.util.*

@Mixin(GameProfile::class)
class GameProfileMixin {

    @Shadow
    private val name: String? = null

    @Inject(at = [At(value = "RETURN")], method = ["getName()Ljava/lang/String;"], cancellable = true)
    fun getName(ci: CallbackInfoReturnable<String>): String {
        return (System.getenv("ROASTY_USERNAME") ?: name ?: "Player335")
    }

}