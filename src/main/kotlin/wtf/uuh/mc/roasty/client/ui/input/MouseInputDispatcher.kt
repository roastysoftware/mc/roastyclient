package wtf.uuh.mc.roasty.client.ui.input

import org.lwjgl.glfw.GLFW
import wtf.uuh.mc.roasty.client.providers.MC
import wtf.uuh.mc.roasty.client.ui.geometry.Box
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import java.util.*

object MouseInputDispatcher {

    private var mousePosition: Point = Point.zero
    private val buttonClickStates = HashMap<MouseButton, Boolean?>()
    private val mouseDownActiveStates =
        HashMap<MouseEventListener, Boolean?>()
    private val mouseListeners =
        ArrayList<MouseEventListener>()

    fun onMousePosChange(mouseX: Double, mouseY: Double) {
        val lastPos = mousePosition
        mousePosition = Point(mouseX.dp, mouseY.dp)
        if (lastPos != mousePosition) {
            dispatchMouseMoveEvent(lastPos)
        }
    }

    fun onMouseButtonStateChange(button: Int, action: Int, mods: Int) {
        val mouseButton = MouseButton.fromGlfwButton(button)
        val wasClicked = if (buttonClickStates.containsKey(mouseButton)) {
            buttonClickStates[mouseButton]
        } else false
        val isClicked = action == GLFW.GLFW_PRESS
        buttonClickStates[mouseButton] = isClicked
        if (wasClicked != isClicked) {
            dispatchMouseClickEvent(mouseButton, isClicked)
        }
    }

    private fun dispatchMouseMoveEvent(lastMousePos: Point, force: Boolean = false) {
        val allTopMostWithin2DBounds = allTopMostWithin2DBounds()
        mouseListeners.forEach { listener ->
            if (!listener.shouldListenToMouse || listener.visibleArea == null) {
                return
            }
            val previouslyWithinBounds = isWithinBounds(listener.visibleArea!!, lastMousePos)
            if (allTopMostWithin2DBounds.contains(listener)) {
                // mouse is withing bounds, so we can notify the listener
                listener.onMouseMove(mousePosition)
                if (!previouslyWithinBounds || force) {
                    // mouse has entered the target bounds
                    listener.onMouseEnter()
                }
            } else {
                if (previouslyWithinBounds || force) {
                    // mouse has left the target bounds
                    listener.onMouseLeave()
                }
            }
        }
    }

    private fun isWithinBounds(bounds: Box, mousePosition: Point): Boolean {
        return mousePosition.x >= bounds.minX && mousePosition.y >= bounds.minY &&
                mousePosition.x <= bounds.maxX && mousePosition.y <= bounds.maxY
    }

    private fun isZIndexWithinBounds(bounds: Box, zIndex: Double): Boolean {
        return zIndex >= bounds.depth && zIndex <= bounds.depth
    }

    private fun dispatchMouseClickEvent(mouseButton: MouseButton, isClicked: Boolean) {
        allTopMostWithin2DBounds().forEach { listener ->
            if (isClicked) {
                mouseDownActiveStates[listener] = true
            }
            listener.onMouseClickStateChange(
                mousePosition, mouseButton, isClicked
            )
            if (!isClicked) {
                listener.onMouseClick(mousePosition, mouseButton)
                mouseDownActiveStates.clear()
            }
        }
    }

    private fun allTopMostWithin2DBounds(): List<MouseEventListener> {
        val topMost = topMostWithin2DBounds() ?: return listOf()
        return allWithin2DBounds().filter { listener ->
            isZIndexWithinBounds(
                listener.visibleArea!!,
                topMost.zIndex
            )
        }
    }

    private fun validListeners(): List<MouseEventListener> {
        return mouseListeners
            .filter(MouseEventListener::shouldListenToMouse)
            .filter { it.visibleArea != null }
    }

    private fun allWithin2DBounds(): List<MouseEventListener> {
        return validListeners()
            .filter { listener ->
                isWithinBounds(listener.visibleArea!!, mousePosition) ||
                        mouseDownActiveStates.getOrDefault(listener, false) ?: false
            }
    }

    private fun topMostWithin2DBounds(): MouseEventListener? {
        return allWithin2DBounds().maxByOrNull { it.zIndex }
    }

    private fun topMostScrollReceiverWithin2DBounds(): MouseEventListener? {
        return allWithin2DBounds().filter { it.isScrollReceiver }.maxByOrNull { it.zIndex }
    }

    fun addMouseListener(listener: MouseEventListener) {
        mouseListeners.add(listener)
    }

    fun removeMouseListener(listener: MouseEventListener) {
        mouseListeners.remove(listener)
    }

    /**
     * Useful when you don't know whether the current state
     * is usable as-is
     */
    fun resetState() {
        mouseDownActiveStates.clear()
        buttonClickStates.clear()
    }

    fun onMouseScroll(horizontal: Double, vertical: Double) {
        topMostScrollReceiverWithin2DBounds()?.let { listener ->
            val listeners = validListeners()
            val boundsStates = listeners.map { isWithinBounds(it.visibleArea!!, mousePosition) }
            listener.onMouseScroll(
                horizontal.dp * MC.options.mouseWheelSensitivity.value.dp.absolute.dp,
                vertical.dp * MC.options.mouseWheelSensitivity.value.dp.absolute.dp
            )
            boundsStates.forEachIndexed { i, wasWithinBounds ->
                val indexListener = listeners[i]
                val isNowWithinBounds = isWithinBounds(indexListener.visibleArea!!, mousePosition)
                if (wasWithinBounds && !isNowWithinBounds) {
                    indexListener.onMouseLeave()
                } else if (!wasWithinBounds && isNowWithinBounds) {
                    indexListener.onMouseEnter()
                }
            }
        }
    }

}
