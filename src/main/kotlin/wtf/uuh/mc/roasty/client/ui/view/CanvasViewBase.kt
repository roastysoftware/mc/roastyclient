package wtf.uuh.mc.roasty.client.ui.view

import wtf.uuh.mc.roasty.client.graphics.opengl.GL.drawTexture
import wtf.uuh.mc.roasty.client.graphics.opengl.GL.uploadImage
import wtf.uuh.mc.roasty.client.ui.drawable.Bitmap
import wtf.uuh.mc.roasty.client.ui.geometry.DP
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.render.TextureContext
import wtf.uuh.mc.roasty.client.ui.render.canvas.Canvas

interface CanvasViewBase {

    var cachedBitmap: Bitmap?
    val invalidated
        get() = cachedBitmap != null
    var textureContext: TextureContext
    val width: DP
    val height: DP
    val pos: Point

    fun draw() {
        if (cachedBitmap == null) {
            cachedBitmap = Canvas(width, height) {
                paint(this)
            }.bitmap.apply bitmap@{
                textureContext = this@CanvasViewBase.textureContext.apply {
                    start { uploadImage(this@bitmap) }
                }
                image = null
            }
        }

        cachedBitmap!!.textureContext?.use {
            drawTexture(cachedBitmap!!, pos)
        }
    }

    fun invalidate() {
        cachedBitmap?.let {
            cachedBitmap = null
        }
    }

    fun onHide() {
        textureContext.release()
    }

    fun onShow() {
        textureContext.arm()
    }

    fun paint(canvas: Canvas)
}
