package wtf.uuh.mc.roasty.client.graphics.opengl

import net.minecraft.client.gui.DrawableHelper
import net.minecraft.client.util.math.MatrixStack
import org.lwjgl.opengl.GL12.*
import wtf.uuh.mc.roasty.client.extensions.then
import wtf.uuh.mc.roasty.client.graphics.opengl.PixelFormatGlue.glFormat
import wtf.uuh.mc.roasty.client.graphics.opengl.PixelFormatGlue.glInternalFormat
import wtf.uuh.mc.roasty.client.graphics.opengl.PixelFormatGlue.glPixelFormatType
import wtf.uuh.mc.roasty.client.logging.logger
import wtf.uuh.mc.roasty.client.providers.MC
import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.drawable.Bitmap
import wtf.uuh.mc.roasty.client.ui.geometry.*
import java.lang.IllegalArgumentException
import java.util.*

const val DEBUG_DISABLE_SCISSOR = false
const val DEBUG_DISABLE_RENDERING = false

object GL {

    fun useStates(caps: IntArray, state: Boolean, runnable: () -> Unit) {
        val beforeStates = BooleanArray(caps.size)
        caps.forEachIndexed { cap, index ->
            beforeStates[index] = glIsEnabled(cap)
            if (beforeStates[index] != state) {
                if (state) {
                    glEnable(cap)
                } else {
                    glDisable(cap)
                }
            }
        }
        try {
            runnable()
        } catch (e: Exception) {
            throw RuntimeException(e)
        } finally {
            caps.forEachIndexed { cap, index ->
                if (beforeStates[index] != state) {
                    if (state) {
                        glDisable(cap)
                    } else {
                        glEnable(cap)
                    }
                }
            }
        }
    }

    inline fun enable(caps: IntArray, crossinline runnable: () -> Unit) {
        useStates(caps, true) {
            runnable()
        }
    }

    inline fun enable(cap: Int, runnable: () -> Unit) {
        val wasEnabled = glIsEnabled(cap)
        if (!wasEnabled) {
            glEnable(cap)
        }
        try {
            runnable()
        } catch (e: Exception) {
            throw RuntimeException(e)
        } finally {
            if (!wasEnabled) {
                glDisable(cap)
            }
        }
    }

    inline fun disable(caps: IntArray, crossinline runnable: () -> Unit) {
        useStates(caps, true) {
            runnable()
        }
    }

    inline fun disable(cap: Int, runnable: () -> Unit) {
        val wasEnabled = glIsEnabled(cap)
        if (wasEnabled) {
            glDisable(cap)
        }
        try {
            runnable()
        } catch (e: Exception) {
            throw RuntimeException(e)
        } finally {
            if (wasEnabled) {
                glEnable(cap)
            }
        }
    }

    val colorStack = Stack<ColorComponents>()

    inline fun color(color: ColorComponents, fn: () -> Unit) {
        glColor4f(color.red, color.green, color.blue, color.alpha)
        colorStack.push(color)
        fn()
        colorStack.empty() then { colorStack.pop() }
        val prevColor = if (colorStack.empty()) ColorComponents.white else colorStack.peek()
        glColor4f(prevColor.red, prevColor.green, prevColor.blue, prevColor.blue)
    }

    inline fun vertices(mode: Int, runnable: () -> Unit) {
        if (DEBUG_DISABLE_RENDERING) {
            return runnable()
        }
        glBegin(mode)
        runnable()
        glEnd()
    }

    inline fun subMatrix(runnable: () -> Unit) {
        if (DEBUG_DISABLE_RENDERING) {
            return runnable()
        }
        glPushMatrix()
        runnable()
        glPopMatrix()
    }

    inline fun mode(mode: Int, runnable: () -> Unit) {
        glBegin(mode)
        runnable()
        glEnd()
    }

    fun disableMipMapping() {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    }

    fun guiScaled() {
        if (DEBUG_DISABLE_RENDERING) {
            return
        }
        val scale = 1.px.dp
        glScaled(scale, scale, 1.0)
    }

    inline fun guiScaled(fn: () -> Unit) {
        if (DEBUG_DISABLE_RENDERING) {
            return
        }
        scaled(1.px) {
            fn()
        }
    }

    fun quadDp(width: DP, height: DP) {
        if (DEBUG_DISABLE_RENDERING) {
            return
        }
        mode(GL_QUADS) {
            glTexCoord2i(0, 0)
            glVertex2d(0.0, 0.0)
            glTexCoord2i(0, 1)
            glVertex2d(0.0, height.dp)
            glTexCoord2i(1, 1)
            glVertex2d(width.dp, height.dp)
            glTexCoord2i(1, 0)
            glVertex2d(width.dp, 0.0)
        }
    }


    fun scaledPositionedQuad(x: DP, y: DP, width: DP, height: DP) {
        if (DEBUG_DISABLE_RENDERING) {
            return
        }
        subMatrix {
            glTranslated(x.dp, y.dp, 0.0)
            quadDp(width, height)
        }
    }

    inline fun translated(point: Point, fn: () -> Unit) {
        subMatrix {
            glTranslated(point.x.dp, point.y.dp, 0.0)
            fn()
        }
    }

    inline fun scaled(x: DP, y: DP = x, fn: () -> Unit) {
        scaled(x.dp, y.dp, fn)
    }

    inline fun scaled(x: Double, y: Double = x, fn: () -> Unit) {
        subMatrix {
            glScaled(x, y, 1.0)
            fn()
        }
    }

    fun uploadImage(bitmap: Bitmap) = bitmap.run {
        if (DEBUG_DISABLE_RENDERING) {
            return
        }
        // reset this to prevent artifacts
        glPixelStorei(GL_UNPACK_SKIP_ROWS, 0)
        disableMipMapping()
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            metadata.format.glInternalFormat,
            width.absInt,
            height.absInt,
            0,
            metadata.format.glFormat,
            metadata.format.glPixelFormatType,
            image!!,
        )
    }

    fun drawTexture(bitmap: Bitmap, pos: Point) {
        if (DEBUG_DISABLE_RENDERING) {
            return
        }
        enable(GL_BLEND) {
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
            bitmap.run {
                scaledPositionedQuad(pos.x, pos.y, width, height)
            }
        }
    }

    inline fun translated(x: DP, y: DP, fn: () -> Unit) {
        if (DEBUG_DISABLE_RENDERING) {
            return fn()
        }
        subMatrix {
            glTranslated(x.absolute, y.absolute, 1.0)
            fn()
        }
    }

    private val scissorStack = Stack<Pair<Point, Rectangle>>()

    private fun applyScissor(pos: Point, rec: Rectangle) {
        if (DEBUG_DISABLE_RENDERING) {
            return
        }
        glScissor(
            pos.x.absInt, MC.window.height - pos.y.absInt - rec.height.absInt,
            rec.width.absInt, rec.height.absInt
        )
    }

    private fun subScissor(pos: Point, rec: Rectangle, fn: () -> Unit) {
        if (DEBUG_DISABLE_RENDERING) {
            return fn()
        }
        if (!scissorStack.empty()) {
            scissorStack.peek().let {
                it.first.run {
                    it.second.run {
                        val nowMaxX = x + width
                        val nowMaxY = y + height
                        if (pos.x < x || pos.y < y || pos.x > nowMaxX || pos.y > nowMaxY) {
                            // Cut off
                            subScissor(pos.copy().clamp(x, y, nowMaxX, nowMaxY), rec, fn)
                            return
                        }
                        val newMaxX = pos.x + rec.width
                        val newMaxY = pos.y + rec.height
                        if (newMaxX > nowMaxX || newMaxY > nowMaxY) {
                            // Cut off the rest of the box
                            subScissor(pos, rec.minus(Rectangle(newMaxX - nowMaxX, newMaxY - nowMaxY)), fn)
                            return
                        }
                    }
                }
            }
        }
        scissorStack.push(Pair(pos, rec))
        applyScissor(pos, rec)
        fn()
        popScissor()
    }

    private fun popScissor() {
        if (DEBUG_DISABLE_RENDERING) {
            return
        }
        if (scissorStack.empty()) {
            glScissor(0, 0, 0, 0)
        } else {
            scissorStack.pop()
            if (!scissorStack.empty()) {
                val prevScissor = scissorStack.peek()
                applyScissor(prevScissor.first, prevScissor.second)
            }
        }
    }

    fun scissor(pos: Point, rec: Rectangle, fn: () -> Unit) {
        if (DEBUG_DISABLE_RENDERING || DEBUG_DISABLE_SCISSOR) {
            return fn()
        }
        subMatrix {
            enable(GL_SCISSOR_TEST) {
                subScissor(pos, rec, fn)
            }
        }
    }

}
