package wtf.uuh.mc.roasty.client.ui.extensions

import net.minecraft.client.MinecraftClient
import kotlin.math.ceil

inline val MinecraftClient.guiScale get() = ceil(window.width / window.scaledWidth.toDouble()).toInt()