package wtf.uuh.mc.roasty.client.ui.geometry

enum class VerticalAlignment {
    TOP, CENTER, BOTTOM
}