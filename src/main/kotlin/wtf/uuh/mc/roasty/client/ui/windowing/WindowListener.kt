package wtf.uuh.mc.roasty.client.ui.windowing

interface WindowListener {
    fun onClose(window: Window)
    fun onOpen(window: Window)
}