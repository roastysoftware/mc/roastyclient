package wtf.uuh.mc.roasty.client.ui.layout

enum class ViewSizing {
    AUTO, FILL_PARENT, FILL
}