package wtf.uuh.mc.roasty.client.ui.windowing

import wtf.uuh.mc.roasty.client.ui.geometry.Point

abstract class WindowManagerBehavior(val windowManager: WindowManager) {
    abstract fun windowPlacementOrientation(): WindowPlacement
    abstract fun nextWindowPosition(window: Window): Point
    abstract fun resetWindowPosition()
}