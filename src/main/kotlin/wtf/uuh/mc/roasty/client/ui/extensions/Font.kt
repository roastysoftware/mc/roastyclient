package wtf.uuh.mc.roasty.client.ui.extensions

import wtf.uuh.mc.roasty.client.providers.MC
import java.awt.Font

fun Font.deriveScaled(size: Float): Font {
    return this.deriveFont(size * MC.guiScale)
}
