package wtf.uuh.mc.roasty.client.ui.view

import net.minecraft.client.gui.DrawableHelper
import wtf.uuh.mc.roasty.client.providers.MC
import wtf.uuh.mc.roasty.client.ui.color.ColorComponents
import wtf.uuh.mc.roasty.client.ui.color.red500
import wtf.uuh.mc.roasty.client.ui.extensions.guiScale
import wtf.uuh.mc.roasty.client.ui.geometry.*
import wtf.uuh.mc.roasty.client.ui.input.*
import wtf.uuh.mc.roasty.client.ui.layout.LayoutParams
import wtf.uuh.mc.roasty.client.ui.layout.ViewSizing
import wtf.uuh.mc.roasty.client.ui.render.DrawContext
import wtf.uuh.mc.roasty.client.ui.windowing.Window
import wtf.uuh.mc.roasty.client.ui.windowing.decorations.SystemTheme
import wtf.uuh.mc.roasty.client.ui.windowing.theming.DefaultTheme
import java.util.*

abstract class View() : MouseEventListener, KeyboardEventListener, Invalidatable {

    open val isInvalidated: Boolean = true

    constructor(init: View.() -> Unit) : this() {
        run(init)
    }

    var isVisible = true
        set(visible) {
            field = visible
            if (visible && isAttachedToWindow) {
                MouseInputDispatcher.addMouseListener(this)
                KeyboardInputDispatcher.addKeyboardEventListener(this)
                onShow()
            } else if (!visible) {
                onHide()
                MouseInputDispatcher.removeMouseListener(this)
                KeyboardInputDispatcher.removeKeyboardEventListener(this)
            }
        }

    var parent: ViewGroup? = null
    inline val parentCount: Int
        get() = if (parent == null) 0 else {
            var p = parent
            var count = 0
            while (p != null) {
                p = p.parent
                count++
            }
            count
        }

    private val clickListeners = ArrayList<OnClickListener>()
    private val secondaryClickListeners = ArrayList<OnSecondaryClickListener>()
    var layoutParams = LayoutParams()

    override val zIndex: Double
        get() = window?.zIndex ?: -1.0

    var window: Window? = null
        protected set

    open val theme: SystemTheme
        get() = parent?.theme ?: DefaultTheme()

    abstract fun draw(context: DrawContext)
    open val height: DP
        get() = when (layoutParams.verticalSizing) {
            ViewSizing.FILL_PARENT -> parent?.innerHeight ?: 0.dp
            ViewSizing.FILL -> parent?.remainingHeight ?: 0.dp
            else -> 0.dp
        } - layoutParams.totalVerticalMargin

    open val width: DP
        get() = when (layoutParams.horizontalSizing) {
            ViewSizing.FILL_PARENT -> parent?.innerWidth ?: 0.dp
            ViewSizing.FILL -> parent?.remainingWidth ?: 0.dp
            else -> 0.dp
        } - layoutParams.totalHorizontalMargin

    open fun onAttachToWindow(window: Window) {
        if (isAttachedToWindow) {
            return
        }
        this.window = window
        if (isVisible) {
            MouseInputDispatcher.addMouseListener(this)
        }
    }

    open fun onDetachFromWindow() {
        if (!isAttachedToWindow) {
            return
        }
        MouseInputDispatcher.removeMouseListener(this)
        window = null
    }

    inline val isAttachedToWindow: Boolean
        get() = window != null

    private inline val outerOffset get() = Point(layoutParams.leftMargin, layoutParams.topMargin)

    inline val outerBounds get() =
        Rectangle(width + layoutParams.totalHorizontalMargin, height + layoutParams.totalVerticalMargin)

    inline val outerPos get() =
        parent!!.getViewPos(this)

    open val pos: Point
        get() = outerPos + outerOffset

    fun addOnClickListener(listener: OnClickListener) {
        clickListeners.add(listener)
    }

    fun removeOnClickListener(listener: OnClickListener) {
        clickListeners.remove(listener)
    }

    fun addOnSecondaryClickListener(listener: OnSecondaryClickListener) {
        secondaryClickListeners.add(listener)
    }

    fun removeOnSecondaryClickListener(listener: OnSecondaryClickListener) {
        secondaryClickListeners.remove(listener)
    }

    open val isEffectivelyVisible: Boolean
        get() = isVisible && parent?.isVisible ?: false

    override fun onMouseClick(mousePosition: Point, button: MouseButton?) {
        when (button) {
            MouseButton.LEFT -> clickListeners.forEach { it() }
            MouseButton.RIGHT -> secondaryClickListeners.forEach { it() }
            else -> {}
        }
    }

    inline val size get() = Rectangle(width, height)

    inline val bounds: Box?
        get() {
            if (window == null) {
                return null
            }
            return Box(pos, size, zIndex)
        }

    override val visibleArea: Box?
        get() = parent?.visibleArea?.let { bounds?.coerceIn(it, it) } ?: bounds

    override val shouldListenToMouse get() = isEffectivelyVisible

    fun onScalingChangedInternal() {
        invalidateSelf()
        invalidate()
        onScalingChanged()
    }

    open fun onHide() {}
    open fun onShow() {}

    open fun onScalingChanged() {

    }

    private fun invalidateSelf() {

    }

    override fun invalidate() {

    }

}

fun View.drawDebugOverlay(context: DrawContext) {
    context.matrixStack.push()
    context.matrixStack.scale(1f/ MC.guiScale, 1f/ MC.guiScale, 1f)
    context.matrixStack.push()
    context.matrixStack.translate(pos.x.absolute, pos.y.absolute, .0)
    DrawableHelper.fill(context.matrixStack, 0, 0, width.absInt, height.absInt,
        ColorComponents(1f, parentCount % 2f, (parentCount + 1) % 2f, .11f).packedColor)
    DrawableHelper.fill(context.matrixStack, 0, 0, width.absInt, 1, red500.packedColor)
    DrawableHelper.fill(context.matrixStack, 0, 0, 1, height.absInt, red500.packedColor)
    context.matrixStack.push()
    context.matrixStack.translate(width.absolute - 1, .0, .0)
    DrawableHelper.fill(context.matrixStack, 0, 0, 1, height.absInt, red500.packedColor)
    context.matrixStack.pop()
    context.matrixStack.push()
    context.matrixStack.translate(.0, height.absolute - 1, .0)
    DrawableHelper.fill(context.matrixStack, 0, 0, width.absInt, 1, red500.packedColor)
    context.matrixStack.pop()
    context.matrixStack.pop()
    context.matrixStack.pop()
}

typealias OnClickListener = () -> Unit
typealias OnSecondaryClickListener = () -> Unit