package wtf.uuh.mc.roasty.client.lib.collections

class ShortDictMap<V> : DictMap<Short?, V>(), OfAble<V, ShortDictMap<V>?> {
    private var next: Short = 0
    override fun add(value: V) {
        put(next++, value)
    }
}