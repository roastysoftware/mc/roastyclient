package wtf.uuh.mc.roasty.client.ui.windowing

import net.minecraft.client.util.math.MatrixStack
import wtf.uuh.mc.roasty.client.ui.geometry.Point
import wtf.uuh.mc.roasty.client.ui.geometry.dp
import wtf.uuh.mc.roasty.client.ui.layout.LinearLayout
import wtf.uuh.mc.roasty.client.ui.layout.ViewSizing.FILL_PARENT
import wtf.uuh.mc.roasty.client.ui.render.DrawContext
import wtf.uuh.mc.roasty.client.ui.view.RootView
import wtf.uuh.mc.roasty.client.ui.view.ScrollView
import wtf.uuh.mc.roasty.client.ui.view.View
import wtf.uuh.mc.roasty.client.ui.view.ViewGroup
import wtf.uuh.mc.roasty.client.ui.widgets.Button
import wtf.uuh.mc.roasty.client.ui.widgets.TextView
import wtf.uuh.mc.roasty.client.ui.windowing.decorations.WindowDecoration
import wtf.uuh.mc.roasty.client.ui.windowing.theming.DefaultTheme
import wtf.uuh.mc.roasty.client.ui.windowing.theming.DefaultWindowDecoration

class Window(val windowManager: WindowManager) {
    companion object {
        val INITIAL_WIDTH = 120.dp
        val INITIAL_HEIGHT = 80.dp
    }

    var isVisible = true
        private set
    val isPinned = false

    private var isReady = false

    val viewGroupProxy = object : ViewGroup() {
        override val height by this@Window::height
        override val width by this@Window::width
        override val innerHeight by this@Window::innerHeight
        override val innerWidth by this@Window::innerWidth
        override val isEffectivelyVisible by this@Window::isEffectivelyVisible
        override val theme = DefaultTheme()
    }

    /**
     * Overwrites existing window manager positioning
     * and is mainly used for allowing the user to move the windows
     */
    private var overwrittenPos: Point? = null
    private var position: Point = Point.zero
    val decoration: WindowDecoration = DefaultWindowDecoration()

    var title: String = "Window"
        set(text) {
            field = text
            windowTitleBar.onTitleChange()
        }

    private val rootView = RootView(this)
    private val contentView = LinearLayout()
    private val windowTitleBar = WindowTitleBar()
    var zIndex = 0.0

    init {
        contentView.layout {
            parameters {
                horizontalSizing = FILL_PARENT
                verticalSizing = FILL_PARENT
            }

            addView(windowTitleBar)
            addView((ScrollView()) {
                addView((LinearLayout()) testLayout@{
                    addView(TextView { text = "Start" })
                    addView(TextView { text = "Test 2" })
                    addView(TextView { text = "Test 3" })
                    addView(Button {
                        text = "Test 4"
                        var bigger = false
                        addOnClickListener {
                            bigger = !bigger
                            textSize = if (bigger) 14.dp else 8.dp
                        }
                    })
                    addView(TextView { text = "Test 5" })
                    addView(Button {
                        text = "Multiline\nTest\npog"
                        var i = 0
                        addOnClickListener {
                            text = "Yes: ${i++}"
                            this@testLayout.addView(TextView{ text = ":o pog"})
                        }
                    })
                    addView(TextView { text = "End" })
                })
            })
        }

        setContentView(contentView)
    }

    private fun onDecorationChange() {
        contentView.layout {
            parameters {
                padding(decoration.windowPadding)
            }
        }
        windowTitleBar.onTitleChange()
        windowTitleBar.isVisible = decoration.shouldShowTitleBar
        windowTitleBar.onDecorationChange()
    }

    /**
     * Makes the window disappear.
     * The window manager will forget about it.
     */
    fun close() {
        windowManager.close(this)
    }

    /**
     * Makes the window appear,
     * the window manager will manage it
     */
    fun open() {
        windowManager.register(this)
    }

    fun prepare() {
        onDecorationChange()
        isReady = true
    }

    fun render(matrixStack: MatrixStack) {
        if (!isReady) {
            prepare()
        }
        applyOverwrittenPosition()
        WindowRenderer.render(
            WindowRenderContext(
                this, matrixStack
            )
        )
        if (canDrawContent) {
            rootView.draw(DrawContext(matrixStack))
        } else if (!rootView.isInvalidated) {
            rootView.invalidate()
        }
    }

    inline val innerWidth get() = width - decoration.windowBorderThickness * 2.0
    inline val innerHeight get() = height - decoration.windowBorderThickness * 2.0

    inline val width get() = INITIAL_WIDTH
    inline val height get() = INITIAL_HEIGHT

    inline val endX get() = pos.x + width
    inline val endY get() = pos.y + height

    var pos: Point
        get() =
            // Clamp so that it always stays within bounds
            position.copy().clamp(
                0.dp, 0.dp,
                (windowManager.width - width).coerceAtLeast(0.dp),
                (windowManager.height - height).coerceAtLeast(0.dp)
            )
        set(pos) {
            position = pos.copy()
        }

    inline val innerOffset get() = Point.zero.add(decoration.windowBorderThickness)

    inline val innerZeroPos: Point
        get() = pos + innerOffset

    inline val isNotPinned: Boolean
        get() = !isPinned

    fun overwritePos(pos: Point) {
        overwrittenPos = pos
    }

    val isPositionOverwritten get() = overwrittenPos != null

    fun applyOverwrittenPosition() {
        if (isPositionOverwritten) {
            pos = overwrittenPos!!
        }
    }

    /**
     * Makes the window visible
     * If you want to open the window, use open()
     */
    fun show() {
        if (!isVisible) {
            isVisible = true
        }
    }

    /**
     * Makes the window invisible
     * If you want to close the window, use close()
     */
    fun hide() {
        if (isVisible) {
            isVisible = false
        }
    }

    private fun setContentView(view: View) {
        rootView.removeView()
        rootView addView view
    }

    private inline val canDrawContent get() = isVisible && contentView.isAttachedToWindow && contentView.isVisible

    inline val isEffectivelyVisible
        get() = isVisible && (windowManager.isVisible || isPinned)

    fun moveToFront() = windowManager.moveToFront(this)
    fun moveToBack() = windowManager.moveToBack(this)

    fun onScalingChanged() {
        rootView.onScalingChangedInternal()
    }

    fun invalidate() {
        rootView.invalidateRecursively()
    }

}