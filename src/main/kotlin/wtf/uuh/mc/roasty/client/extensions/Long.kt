package wtf.uuh.mc.roasty.client.extensions

infix fun Long.msHavePassed(time: Long): Boolean =
    this + time <= System.currentTimeMillis()

infix fun Long.msHaveNotPassed(time: Long): Boolean =
    this + time > System.currentTimeMillis()