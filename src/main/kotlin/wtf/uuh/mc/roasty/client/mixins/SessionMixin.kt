package wtf.uuh.mc.roasty.client.mixins

import net.minecraft.client.gui.hud.InGameHud
import net.minecraft.client.util.Session
import net.minecraft.client.util.math.MatrixStack
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.Shadow
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable
import wtf.uuh.mc.roasty.client.inGameHudLoop
import wtf.uuh.mc.roasty.client.logging.logger
import wtf.uuh.mc.roasty.client.ui.windowing.WindowManager
import java.util.*

@Mixin(Session::class)
class SessionMixin {

    @Shadow
    private val username: String? = null

    @Inject(at = [At(value = "RETURN")], method = ["getUsername()Ljava/lang/String;"], cancellable = true)
    fun getUsername(ci: CallbackInfoReturnable<String>): String? {
        return System.getenv("ROASTY_USERNAME") ?: username
    }

}