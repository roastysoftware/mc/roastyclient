package wtf.uuh.mc.roasty.client.exceptions

import wtf.uuh.mc.roasty.client.graphics.PixelFormat

class UnsupportedPixelFormatException(format: PixelFormat) :
    UnsupportedFormatException("Unsupported pixel format $format")
