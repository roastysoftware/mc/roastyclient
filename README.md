# RoastyClient

An extremely roasty client written in Kotlin

## License

This software and its source code is licensed under the GPL Version 3.0
license only. You may use, study, modify and distribute this program and
its source code according to the license found in the COPYING file.

All files are licensed under the same GPLv3 license except for those in
which a license text can be found at the very beginning.

Following notice applies to all source code except for such explicitly
specified otherwise.

```
    RoastyClient – a block game client
    Copyright (C) 2020  Simão Gomes Viana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

